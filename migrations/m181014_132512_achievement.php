<?php

use yii\db\Migration;
use app\models\AR\TextBuild;
use app\components\traits\TextTypesTrait;

class m181014_132512_achievement extends Migration
{
    use TextTypesTrait;

    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%achievement}}', [
            'id'       => $this->primaryKey(),
            'name'     => $this->text()->notNull(),
            'text'     => $this->text()->notNull(),
        ], $tableOptions);
        
        $this->createTable('{{%achievement_user}}', [
            'id'                => $this->primaryKey(),
            'achievement_id'    => $this->integer(),
            'user_id'           => $this->integer(),
            'date'              => $this->integer(),
        ], $tableOptions);
        
        $this->addForeignKey('fk_user_ach', '{{%achievement_user}}', 'achievement_id', '{{%achievement}}', 'id','CASCADE');
        $this->addForeignKey('fk_ach_user', '{{%achievement_user}}', 'user_id', '{{%user}}', 'id','CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%achievement_user}}');
        $this->dropTable('{{%achievement}}');
    }
}
