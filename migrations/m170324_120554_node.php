<?php

use yii\db\Migration;

class m170324_120554_node extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%node}}', [
            'id'        => $this->primaryKey(),
            'title'     => $this->string()->notNull(),
            'parent_id' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_node_self', '{{%node}}', 'parent_id', '{{%node}}', 'id');

        $this->createTable('{{%node_tag}}', [
            'id'      => $this->primaryKey(),
            'node_id' => $this->integer()->notNull(),
            'tag_id'  => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_node_tag', '{{%node_tag}}', 'node_id', '{{%node}}', 'id');
        $this->addForeignKey('fk_tag_node', '{{%node_tag}}', 'tag_id', '{{%tag}}', 'id');
    }

    public function safeDown()
    {
        $tables = [
            '{{%node_tag}}',
            '{{%node}}',
        ];

        foreach ($tables as $table) {
            $this->dropTable($table);
        }
    }
}