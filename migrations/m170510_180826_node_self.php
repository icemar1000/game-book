<?php

use yii\db\Migration;
use app\models\AR\Node;

class m170510_180826_node_self extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%node_node}}', [
            'id'        => $this->primaryKey(),
            'parent_id' => $this->integer()->notNull(),
            'child_id'  => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_node_parent', '{{%node_node}}', 'parent_id', '{{%node}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_node_child', '{{%node_node}}', 'child_id', '{{%node}}', 'id', 'CASCADE', 'CASCADE');

        $nodes = Node::find()
            ->with(['parent'])
            ->all();

        foreach ($nodes as $node) {
            /* @var $node app\models\AR\Node */
            if ($node->parent) {
                $node->link('parents', $node->parent);
            }
        }

        $this->dropForeignKey('fk_node_self', '{{%node}}');
        $this->dropColumn('{{%node}}', 'parent_id');
    }

    public function safeDown()
    {
        echo "m170510_180826_node_self cannot be reverted.\n";

        return false;
    }
}