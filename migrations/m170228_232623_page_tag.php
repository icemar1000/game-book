<?php

use yii\db\Migration;

class m170228_232623_page_tag extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tag}}', [
            'id'       => $this->primaryKey(),
            'title'    => $this->string()->notNull(),
            'category' => $this->string()->notNull(),
            'property' => $this->string()->notNull(),
            'value'    => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%page_tag}}', [
            'id'       => $this->primaryKey(),
            'page_id'  => $this->integer()->notNull(),
            'tag_id'   => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_page_tag', '{{%page_tag}}', 'page_id', '{{%page}}', 'id');
        $this->addForeignKey('fk_tag_page', '{{%page_tag}}', 'tag_id', '{{%tag}}', 'id');

        $this->createTable('{{%action_tag}}', [
            'id'         => $this->primaryKey(),
            'action_id'  => $this->integer()->notNull(),
            'tag_id'     => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_action_tag', '{{%action_tag}}', 'action_id', '{{%action}}', 'id');
        $this->addForeignKey('fk_tag_action', '{{%action_tag}}', 'tag_id', '{{%tag}}', 'id');
    }

    public function safeDown()
    {
        $tables = [
            '{{%action_tag}}',
            '{{%page_tag}}',
            '{{%tag}}',
        ];

        foreach ($tables as $table) {
            $this->dropTable($table);
        }
    }
}