<?php

use yii\db\Migration;

class m170304_012429_trigger_title extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%trigger}}', 'title', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%trigger}}', 'title');
    }
}