<?php

use yii\db\Migration;
use app\components\traits\TextTypesTrait;

class m180102_145806_game_details extends Migration
{
    use TextTypesTrait;

    public function safeUp()
    {
        $this->addColumn('{{%game}}', 'data', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%game}}', 'data');
    }
}