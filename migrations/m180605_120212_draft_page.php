<?php

use yii\db\Migration;
use app\models\AR\TextBuild;
use app\components\traits\TextTypesTrait;

class m180605_120212_draft_page extends Migration
{
        use TextTypesTrait;

    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page_draft}}', [
            'id'       => $this->primaryKey(),
            'page_id'  => $this->integer(),
            'text'     => $this->text()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%page_draft}}');
    }
}
