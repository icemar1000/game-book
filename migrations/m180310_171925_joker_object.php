<?php

use yii\db\Migration;
use app\components\traits\TextTypesTrait;

class m180310_171925_joker_object extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%object}}', [
            'category' => 'Карта',
            'title' => 'joker',
            'text' => 'Джокер',
        ]);
    }

    public function safeDown()
    {
        $this->delete("{{%object}}", [
            'title' => 'joker',
        ]);
    }
}