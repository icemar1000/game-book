<?php

use yii\db\Migration;
use app\components\traits\TextTypesTrait;

class m180224_180709_game_objects_FK extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('fk_game', '{{%game_object}}', 'game_id', '{{%game}}', 'id','CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_game', '{{%game_object}}');
    }
}