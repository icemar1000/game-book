<?php

use yii\db\Migration;
use app\models\AR\BugReport;

class m180224_180933_bug_report extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bug_report}}', [
            'id' => $this->primaryKey(),
            'at' => $this->integer()->notNull(),
            'page_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'message' => $this->text()->notNull(),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(BugReport::STATUS_ACTUAL),
        ], $tableOptions);

        $this->addForeignKey('fk_bug_report_page', '{{%bug_report}}', 'page_id', '{{%page}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_bug_report_user', '{{%bug_report}}', 'user_id', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%bug_report}}');
    }
}