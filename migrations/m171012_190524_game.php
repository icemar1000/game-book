<?php

use yii\db\Migration;
use app\models\AR\TextBuild;
use app\components\traits\TextTypesTrait;

class m171012_190524_game extends Migration
{
    use TextTypesTrait;

    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%game}}', [
            'id'       => $this->primaryKey(),
            'page_id'     => $this->integer(),
            'user_id'     => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_user_game', '{{%game}}', 'user_id', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%game}}');
    }
}