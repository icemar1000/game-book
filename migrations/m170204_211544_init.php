<?php

use yii\db\Migration;
use app\models\AR\User;

class m170204_211544_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(User::AUTH_KEY_LENGTH)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(User::STATUS_HOLD),
            'role' => $this->string()->notNull()->defaultValue(User::ROLE_PLAYER),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->notNull()->defaultValue('default'),
            'title' => $this->string(),
            'text' => $this->text()->notNull(),
            'x' => $this->integer(),
            'y' => $this->integer(),
        ], $tableOptions);

        $this->createTable('{{%object}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->notNull()->defaultValue('default'),
            'title' => $this->string()->notNull(),
            'text' => $this->text(),
            'value' => $this->integer(),
            'parent_id' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_object_self', '{{%object}}', 'parent_id', '{{%object}}', 'id');

        $this->createTable('{{%action}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()->notNull()->defaultValue('default'),
            'text' => $this->text()->notNull(),
            'condition' => $this->text(),
            'callback' => $this->text(),
            'index' => $this->integer()->notNull()->defaultValue(1),
            'from_id' => $this->integer(),
            'to_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_from', '{{%action}}', 'from_id', '{{%page}}', 'id');
        $this->addForeignKey('fk_to', '{{%action}}', 'to_id', '{{%page}}', 'id');

        $this->createTable('{{%trigger}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text(),
            'condition' => $this->text(),
            'callback' => $this->text(),
            'priority' => $this->integer()->notNull()->defaultValue(1),
            'page_id' => $this->integer(),
        ], $tableOptions);
        $this->addForeignKey('fk_event', '{{%trigger}}', 'page_id', '{{%page}}', 'id');
    }

    public function safeDown()
    {
        $tables = [
            '{{%trigger}}',
            '{{%action}}',
            '{{%object}}',
            '{{%page}}',
            '{{%user}}',
        ];

        foreach ($tables as $table) {
            $this->dropTable($table);
        }
    }
}