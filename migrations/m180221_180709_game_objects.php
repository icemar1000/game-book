<?php

use yii\db\Migration;
use app\components\traits\TextTypesTrait;

class m180221_180709_game_objects extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%game_object}}', [
            'id'       => $this->primaryKey(),
            'object_id'     => $this->integer(),
            'game_id'     => $this->integer(),
            'value'     => $this->integer(),
            'place'     => $this->string(),
        ], $tableOptions);
        
        $model = new app\models\AR\Object();
        $model->category = app\models\AR\Object::$CATEGORY_CARD;
        $model->title = app\models\AR\Object::$CARD_SUIT_CLUB;
        $model->text = 'Трефы';
        $model->save();
        
        $model = new app\models\AR\Object();
        $model->category = app\models\AR\Object::$CATEGORY_CARD;
        $model->title = app\models\AR\Object::$CARD_SUIT_DIAMOND;
        $model->text = 'Бубны';
        $model->save();
        
        $model = new app\models\AR\Object();
        $model->category = app\models\AR\Object::$CATEGORY_CARD;
        $model->title = app\models\AR\Object::$CARD_SUIT_HEART;
        $model->text = 'Черви';
        $model->save();
        
        $model = new app\models\AR\Object();
        $model->category = app\models\AR\Object::$CATEGORY_CARD;
        $model->title = app\models\AR\Object::$CARD_SUIT_SPADE;
        $model->text = 'Пики';
        $model->save();
        
        $model = new app\models\AR\Object();
        $model->category = app\models\AR\Object::$CATEGORY_MONEY;
        $model->title = app\models\AR\Object::$MONEY;
        $model->text = 'Кредиты';
        $model->save();
        
    }

    public function safeDown()
    {
        $this->dropTable('{{%game_object}}');
    }
}