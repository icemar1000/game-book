<?php

use yii\db\Migration;

class m170324_123316_admin_log extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%admin_log}}', [
            'id'       => $this->primaryKey(),
            'at'       => $this->integer()->notNull(),
            'user_id'  => $this->integer()->notNull(),
            'model'    => $this->string()->notNull(),
            'action'   => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%admin_log}}');
    }
}