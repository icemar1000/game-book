<?php

use yii\db\Migration;

class m170409_012936_node_coordinates extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%node}}', 'x', $this->integer());
        $this->addColumn('{{%node}}', 'y', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%node}}', 'x');
        $this->dropColumn('{{%node}}', 'y');
    }
}