<?php

use yii\db\Migration;
use app\components\traits\TextTypesTrait;

class m180224_200709_trigger_only_one extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%trigger}}', 'ones', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%trigger}}', 'ones');
    }
}