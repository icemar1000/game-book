<?php

use yii\db\Migration;
use app\models\AR\TextBuild;
use app\components\traits\TextTypesTrait;

class m170625_190624_text_build extends Migration
{
    use TextTypesTrait;

    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%text_build}}', [
            'id'       => $this->primaryKey(),
            'title'    => $this->string()->notNull(),
            'text'     => $this->longText()->notNull(),
            'settings' => $this->text(),
            'status'   => $this->smallInteger(2)->notNull()->defaultValue(TextBuild::STATUS_IN_WORK),
        ], $tableOptions);

        $this->addColumn('{{%page}}', 'number', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropTable('{{%text_build}}');
        $this->dropColumn('{{%page}}', 'number');
    }
}