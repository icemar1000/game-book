<?php

use yii\db\Migration;

class m170409_005146_tag_fk extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_page_tag', '{{%page_tag}}');
        $this->dropForeignKey('fk_tag_page', '{{%page_tag}}');
        $this->dropForeignKey('fk_action_tag', '{{%action_tag}}');
        $this->dropForeignKey('fk_tag_action', '{{%action_tag}}');
        $this->dropForeignKey('fk_node_tag', '{{%node_tag}}');
        $this->dropForeignKey('fk_tag_node', '{{%node_tag}}');

        $this->dropForeignKey('fk_node_self', '{{%node}}');

        $this->addForeignKey('fk_page_tag', '{{%page_tag}}', 'page_id', '{{%page}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tag_page', '{{%page_tag}}', 'tag_id', '{{%tag}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_action_tag', '{{%action_tag}}', 'action_id', '{{%action}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tag_action', '{{%action_tag}}', 'tag_id', '{{%tag}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_node_tag', '{{%node_tag}}', 'node_id', '{{%node}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tag_node', '{{%node_tag}}', 'tag_id', '{{%tag}}', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_node_self', '{{%node}}', 'parent_id', '{{%node}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_page_tag', '{{%page_tag}}');
        $this->dropForeignKey('fk_tag_page', '{{%page_tag}}');
        $this->dropForeignKey('fk_action_tag', '{{%action_tag}}');
        $this->dropForeignKey('fk_tag_action', '{{%action_tag}}');
        $this->dropForeignKey('fk_node_tag', '{{%node_tag}}');
        $this->dropForeignKey('fk_tag_node', '{{%node_tag}}');

        $this->dropForeignKey('fk_node_self', '{{%node}}');

        $this->addForeignKey('fk_page_tag', '{{%page_tag}}', 'page_id', '{{%page}}', 'id');
        $this->addForeignKey('fk_tag_page', '{{%page_tag}}', 'tag_id', '{{%tag}}', 'id');
        $this->addForeignKey('fk_action_tag', '{{%action_tag}}', 'action_id', '{{%action}}', 'id');
        $this->addForeignKey('fk_tag_action', '{{%action_tag}}', 'tag_id', '{{%tag}}', 'id');
        $this->addForeignKey('fk_node_tag', '{{%node_tag}}', 'node_id', '{{%node}}', 'id');
        $this->addForeignKey('fk_tag_node', '{{%node_tag}}', 'tag_id', '{{%tag}}', 'id');

        $this->addForeignKey('fk_node_self', '{{%node}}', 'parent_id', '{{%node}}', 'id');
    }
}