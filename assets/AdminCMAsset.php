<?php

namespace app\assets;

use yii\web\AssetBundle;
use conquer\codemirror\CodemirrorAsset;

class AdminCMAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    public $css = [
        'css/cm-material.css',
    ];

    /**
     * @inheritdoc
     */
    public static function register($view) {
        CodemirrorAsset::register($view, [
            CodemirrorAsset::MODE_JAVASCRIPT,
            CodemirrorAsset::KEYMAP_EMACS,
            CodemirrorAsset::ADDON_EDIT_MATCHBRACKETS,
            CodemirrorAsset::ADDON_COMMENT,
            CodemirrorAsset::ADDON_DIALOG,
            CodemirrorAsset::ADDON_SEARCHCURSOR,
            CodemirrorAsset::ADDON_SEARCH,
            CodemirrorAsset::THEME_YETI,
        ]);
        return parent::register($view);
    }
}
