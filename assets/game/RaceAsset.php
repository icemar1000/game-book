<?php

namespace app\assets\game;

use yii\web\AssetBundle;

class RaceAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    public $css = [
        'css/game/race.css',
    ];
    public $js = [
        'js/game/race.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public static function getCssFile() {
        return '@app/modules/admin/assets/css/game/race.css';
    }
}
