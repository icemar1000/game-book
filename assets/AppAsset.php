<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-solar.min.css',
        'css/site.css',
        'css/game.css',
        'https://fonts.googleapis.com/css?family=Inconsolata:400,700|Roboto+Condensed:300,400,700&amp;subset=cyrillic',
    ];
    public $js = [];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
