<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminVisAsset extends AssetBundle
{
    public $sourcePath = '@bower/vis/dist';
    public $js = [
        'vis.min.js',
    ];
    public $css = [
        'vis-network.min.css',
    ];
}
