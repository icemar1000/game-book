<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminGraphAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';
    public $js = [
        'js/graph.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'app\assets\AdminVisAsset',
    ];
}
