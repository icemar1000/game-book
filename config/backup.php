<?php

return [
    'class' => 'demi\backup\Component',
    'backupsFolder' => dirname(__DIR__) . '/backups',
    'expireTime' => 7 * 24 * 60 * 60,
];
