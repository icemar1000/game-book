<?php

use yii\helpers\Url;
use kartik\date\DatePicker;

$array = [
    'yii\grid\GridView' => [
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
    ],
    'yii\grid\DataColumn' => [
        'filterInputOptions' => [
            'class' => 'form-control input-sm',
        ],
    ],
    'kartik\select2\Select2' => [
        'language'      => 'ru',
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ],
    'kartik\date\DatePicker' => [
        'language'      => 'en',
        'type'          => DatePicker::TYPE_INPUT,
        'options'       => [
            'class'     => 'form-control input-sm',
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'format'    => 'yyyy-mm-dd',
        ],
    ],
    'app\components\widgets\imperavi\Widget' => [
        'settings' => [
            'lang'             => 'ru',
            'fileUpload'       => Url::to(['/admin/ajax/file-upload']),
            'imageUpload'      => Url::to(['/admin/ajax/image-upload']),
            'fileManagerJson'  => Url::to(['/admin/ajax/files-get']),
            'imageManagerJson' => Url::to(['/admin/ajax/images-get']),
            'plugins'          => [
                'fontfamily',
                'fontsize',
                'fontcolor',
                'imagemanager',
                'filemanager',
                'video',
                'table',
                'clips',
                'fullscreen',
            ],
        ],
    ],
    'conquer\codemirror\CodemirrorWidget' => [
        'settings' => [
            'lineNumbers'     => true,
            'matchBrackets'   => true,
            'styleActiveLine' => true,
            'indentUnit'      => 4,
            'mode'            => 'javascript',
            'theme'           => 'material',
        ],
    ],
];

foreach ($array as $key => $value) {
    Yii::$container->set($key, $value);
}