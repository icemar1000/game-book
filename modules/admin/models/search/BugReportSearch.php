<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\models\AR\BugReport;

/**
 * BugReportSearch represents the model behind the search form about `app\models\AR\BugReport`.
 */
class BugReportSearch extends BugReport
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        ActiveRecord::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_id', 'user_id', 'status'], 'integer'],
            [['at'], 'date', 'format' => 'php:Y-m-d'],
            [['message'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BugReport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'status' => SORT_ASC,
                    'at'     => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'      => $this->id,
            'page_id' => $this->page_id,
            'user_id' => $this->user_id,
            'status'  => $this->status,
        ]);

        if ($this->at) {
            $query->andFilterWhere([
                'between',
                'at',
                strtotime($this->at),
                strtotime($this->at) + 86399,
            ]);
        }

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
