<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\models\AR\Node;

/**
 * NodeSearch represents the model behind the search form about `app\models\AR\Node`.
 */
class NodeSearch extends Node
{
    public $tag_ids;
    public $parent_ids;
    public $children_ids;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->detachBehavior('flashMessages');
        $this->detachBehavior('manyToMany');
        ActiveRecord::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['tag_ids', 'parent_ids', 'children_ids'], 'each', 'rule' => ['integer']],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $defaultPageSize
     *
     * @return ActiveDataProvider
     */
    public function search($params, $defaultPageSize = 50)
    {
        $query = Node::find()
            ->distinct()
            ->joinWith([
                'tags',
                'parents',
                'children',
            ]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'defaultPageSize' => $defaultPageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['node.id' => $this->id]);

        if (!empty($this->tag_ids)) {
            $query->andFilterWhere(['in', 'tag.id', $this->tag_ids]);
        }

        if (!empty($this->parent_ids)) {
            $query->andFilterWhere(['in', 'parents.id', $this->parent_ids]);
        }

        if (!empty($this->children_ids)) {
            $query->andFilterWhere(['in', 'children.id', $this->children_ids]);
        }

        $query->andFilterWhere(['like', 'node.title', $this->title]);

        return $dataProvider;
    }
}
