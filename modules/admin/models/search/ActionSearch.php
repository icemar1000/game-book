<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\models\AR\Action;

/**
 * ActionSearch represents the model behind the search form about `app\models\AR\Action`.
 */
class ActionSearch extends Action
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        ActiveRecord::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'index', 'from_id', 'to_id'], 'integer'],
            [['category', 'text', 'condition', 'callback'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Action::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'index' => $this->index,
            'from_id' => $this->from_id,
            'to_id' => $this->to_id,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'condition', $this->condition])
            ->andFilterWhere(['like', 'callback', $this->callback]);

        return $dataProvider;
    }
}
