<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\models\AR\Page;
use app\models\AR\Action;

/**
 * PageSearch represents the model behind the search form about `app\models\AR\Page`.
 */
class PageSearch extends Page
{
    public $tag_ids;
    public $input_ids;
    public $output_ids;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->detachBehavior('relatedModels');
        $this->detachBehavior('flashMessages');
        $this->detachBehavior('manyToMany');
        ActiveRecord::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number'], 'integer'],
            [['tag_ids', 'input_ids', 'output_ids'], 'each', 'rule' => ['integer']],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $defaultPageSize
     *
     * @return ActiveDataProvider
     */
    public function search($params, $defaultPageSize = 50)
    {
        $query = Page::find()
            ->distinct()
            ->joinWith([
                'tags',
                'inputLinks' => function ($query) {
                    /* @var $query \yii\db\ActiveQuery */
                    $query->from(['input' => Action::tableName()]);
                },
                'outputLinks' => function ($query) {
                    /* @var $query \yii\db\ActiveQuery */
                    $query->from(['output' => Action::tableName()]);
                },
            ]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => $defaultPageSize,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'page.id'     => $this->id,
            'page.number' => $this->number,
        ]);

        if (!empty($this->tag_ids)) {
            $query->andFilterWhere(['in', 'tag.id', $this->tag_ids]);
        }

        if (!empty($this->input_ids)) {
            $query->andFilterWhere(['in', 'input.from_id', $this->input_ids]);
        }

        if (!empty($this->output_ids)) {
            $query->andFilterWhere(['in', 'output.to_id', $this->output_ids]);
        }

        $query->andFilterWhere(['like', 'page.title', $this->title]);

        return $dataProvider;
    }
}
