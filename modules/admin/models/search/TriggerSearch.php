<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\models\AR\Trigger;

/**
 * TriggerSearch represents the model behind the search form about `app\models\AR\Trigger`.
 */
class TriggerSearch extends Trigger
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        ActiveRecord::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'priority', 'page_id'], 'integer'],
            [['title', 'text', 'condition', 'callback'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trigger::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'       => $this->id,
            'priority' => $this->priority,
            'page_id'  => $this->page_id,
        ]);

        $query
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'condition', $this->condition])
            ->andFilterWhere(['like', 'callback', $this->callback]);

        return $dataProvider;
    }
}
