<?php

namespace app\modules\admin\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AR\AdminLog;

/**
 * AdminLogSearch represents the model behind the search form about `app\models\AR\AdminLog`.
 */
class AdminLogSearch extends AdminLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'model_id'], 'integer'],
            [['at'], 'date', 'format' => 'php:Y-m-d'],
            [['action', 'model'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'       => $this->id,
            'model'    => $this->model,
            'action'   => $this->action,
            'user_id'  => $this->user_id,
            'model_id' => $this->model_id,
        ]);

        if ($this->at) {
            $query->andFilterWhere([
                'between',
                'at',
                strtotime($this->at),
                strtotime($this->at) + 86399,
            ]);
        }

        return $dataProvider;
    }
}
