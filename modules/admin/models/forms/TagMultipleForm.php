<?php

namespace app\modules\admin\models\forms;

use Yii;
use yii\base\Model;
use yii\base\InvalidConfigException;

class TagMultipleForm extends Model
{
    public $ids          = [];
    public $tag_ids_add  = [];
    public $tag_ids_drop = [];
    public $entity       = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['ids', 'tag_ids_add', 'tag_ids_drop'],
                'each',
                'rule' => ['integer'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'ids',
                'tag_ids_drop',
                'tag_ids_add',
                '!entity',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ids'          => 'Страницы',
            'tag_ids_add'  => 'Добавить теги',
            'tag_ids_drop' => 'Убрать теги',
        ];
    }

    /**
     * @return boolean
     */
    public function apply()
    {
        if ($this->entity === null) {
            throw new InvalidConfigException('Entity not set.');
        }

        if (!$this->validate()) {
            return false;
        }

        $db = Yii::$app->db;
        $add = !empty($this->tag_ids_add) ? $this->tag_ids_add : [];
        $insert = [];

        foreach ($this->ids as $node) {
            foreach ($add as $tag) {
                $insert[] = [$node, $tag];
            }
        }

        $db
            ->createCommand()
            ->batchInsert("{{%{$this->entity}_tag}}", [
                "{$this->entity}_id",
                "tag_id",
            ], $insert)
            ->execute();

        $db
            ->createCommand()
            ->delete("{{%{$this->entity}_tag}}", [
                "{$this->entity}_id" => $this->ids,
                "tag_id"             => $this->tag_ids_drop,
            ])
            ->execute();

        return true;
    }
}