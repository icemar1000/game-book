<?php

namespace app\modules\admin\models\forms;

use Yii;
use app\models\AR\User;
use yii\base\Model;
use yii\base\UserException;

class ProfileChangePasswordForm extends Model
{
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            [['password'], 'string', 'min' => 6, 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * @return boolean whether the email was send
     */
    public function changePassword()
    {
        if (Yii::$app->user->isGuest) {
            throw new UserException('Логин не выполнен.');
        }

        $user = User::findOne(Yii::$app->user->identity->id);
        $user->setPassword($this->password);
        return $user->save();
    }
}
