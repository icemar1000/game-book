$(document).ready(function () {
    $('#graph-modal').on('hidden.bs.modal', function () {
        $('#graph-modal .modal-title').html('');
        $('#graph-modal .modal-body').html('');
        $('#graph-modal .update-model').attr('href', '#');
    });

    $(document).on('ifChecked', '#selectAllPageTags', function () {
        $('.tag-page-iCheck').iCheck('check');
    });

    $(document).on('ifChecked', '#selectAllLinkTags', function () {
        $('.tag-link-iCheck').iCheck('check');
    });

    $(document).on('ifUnchecked', '#selectAllPageTags', function () {
        $('.tag-page-iCheck').iCheck('uncheck');
    });

    $(document).on('ifUnchecked', '#selectAllLinkTags', function () {
        $('.tag-link-iCheck').iCheck('uncheck');
    });

    draw();
});

var network = null;

function destroy() {
    if (network !== null) {
        network.destroy();
        network = null;
    }
}

function draw() {
    destroy();

    var container = $('#network');

    var setNodeCoordsUrl = container.attr('data-url-set-node-coordinates');
    var getDataUrl       = container.attr('data-url-get-graph');
    var getNodeUrl       = container.attr('data-url-get-node');
    var getEdgeUrl       = container.attr('data-url-get-edge');

    $.ajax({
        url: getDataUrl,
        type: 'GET',
        success: function (response) {
            var container = document.getElementById('network');

            var nodes = new vis.DataSet(response.nodes);
            var edges = new vis.DataSet(response.edges);

            var data = {
                nodes: nodes,
                edges: edges
            };

            var options = {
                height: 'calc(100vh - 180px)',
                nodes: {
                    shape: 'dot'
                },
                interaction: {
                    navigationButtons: true,
                    keyboard: true
                },
                layout: {
                    improvedLayout: false
                },
                physics: {
                    enabled: false
                }
            };

            network = new vis.Network(container, data, options);

            if (getNodeUrl || getEdgeUrl) {
                network.on('doubleClick', function (event) {
                    var id  = null;
                    var url = null;

                    if (event.nodes.length > 0) {
                        id  = event.nodes[0];
                        url = getNodeUrl;
                    } else if (event.edges.length > 0) {
                        id  = event.edges[0];
                        url = getEdgeUrl;
                    }

                    if (url !== null) {
                        $.ajax({
                            url: url,
                            type: 'GET',
                            data: {
                                id: id
                            },
                            success: function (response) {
                                $('#graph-modal .modal-title').html(response.title);
                                $('#graph-modal .modal-body').html(response.body);
                                $('#graph-modal .update-model').attr('href', response.url);

                                $('#graph-modal').modal('show');
                            }
                        });
                    }
                });
            }

            network.on('dragEnd', function (event) {
                if (event.nodes.length > 0) {
                    network.storePositions();

                    var positions = network.getPositions(event.nodes);
                    var id = event.nodes[0];

                    $.ajax({
                        url: setNodeCoordsUrl,
                        type: 'GET',
                        data: {
                            id: id,
                            x: positions[id].x,
                            y: positions[id].y
                        }
                    });
                }
            });
        }
    });
}