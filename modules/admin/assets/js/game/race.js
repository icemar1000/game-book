$(function () {
    $(document).on('click', '.page-links a', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        console.log(id);

        $('.page').removeClass('active');

        $('html, body').animate({
            scrollTop: 0
        }, 100, function () {
            $('html, body').animate({
                scrollTop: $('.page[data-id=' + id + ']').offset().top
            }, 500, function () {
                $('.page[data-id=' + id + ']').addClass('active');
            });
        });
    });
});