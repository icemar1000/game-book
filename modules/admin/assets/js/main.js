$(document).ready(function () {
    var root = $('html, body');
    var iCheck = 'square-blue';

    $(document).on('ifChecked', '.iCheckSelectAll', function () {
        $($(this).attr('data-select')).iCheck('check');
    });

    $(document).on('keyup', '.field-page-text', function () {
        $.ajax({
            url: $('.page-editor').attr('data-autosave'),
            data: {
                text: $('.field-page-text .redactor-editor').html(),
            },
            type: 'POST',
            success: function (data) {

            }
        });
    });

    $(document).on('ifUnchecked', '.iCheckSelectAll', function () {
        $($(this).attr('data-select')).iCheck('uncheck');
    });

    if ($('.box').length) {
        console.log($.Ad)
        $.AdminLTE.boxWidget.remove = function (element) {
            var box = element.parents(".box").first();
            box.slideUp(this.animationSpeed, function () {
                box.remove();
            });
        };

        $.AdminLTE.options.boxWidgetOptions.boxWidgetIcons.collapse = 'fa-chevron-up';
        $.AdminLTE.options.boxWidgetOptions.boxWidgetIcons.open     = 'fa-chevron-down';
    }

    window.setTimeout(function () {
        $(".content > .alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove(); 
        });
    }, 3000);

    $('.password-input .generate').on({
        click: function () {
            var password = $.passGen({
                'length': 8,
                'numeric': false
            });
            $('.password-hint').html('<code>' + password + '</code>');
            $('.password-input input').val(password);
        }
    });

    $('.addRelation').on({
        click: function () {
            var button = this;
            var target = $(button).attr('data-target');
            var maxKey = 0;
            $(target + ' .form-partition').each(function () {
                var key = parseInt($(this).attr('data-key'));
                if (key > maxKey) {
                    maxKey = key;
                }
            });
            var key = maxKey + 1;
            $.ajax({
                url: $(button).attr('data-url'),
                data: {
                    key: key
                },
                type: 'GET',
                success: function (data) {
                    var selector = target + " .form-partition[data-key='" + key + "']";
                    $(target).append(data);
                    root.animate({
                        scrollTop: $(selector).offset().top
                    }, 600);
                }
            });
        }
    });

    $(document).on('click', 'a.to-anchor', function (event) {
        event.preventDefault();
        var name = $.attr(this, 'href').substr(1);
        root.animate({
            scrollTop: $('[name="' + name + '"]').offset().top
        }, 600);
    });

    $(document).on('click', '.form-partition-wrapper .box-header', function () {
        $(this).parent().toggleBox();
    });

    $('#TagMultipleForm').on({
        submit: function () {
            $('.hiddenCheckboxes').html('');
            var selector = '.modelCheckboxWrapper .icheckbox_' + iCheck + '.checked .model-iCheck';
            $(selector).each(function () {
                $(this).clone().appendTo('.hiddenCheckboxes');
            });
            $('.hiddenCheckboxes .model-iCheck').attr('style', '');
            $('.hiddenCheckboxes .model-iCheck').attr('checked', true);
        }
    });

    $(document).on('ifChecked', '#selectAllModels', function () {
        $('.model-iCheck').iCheck('check');
    });

    $(document).on('ifUnchecked', '#selectAllModels', function () {
        $('.model-iCheck').iCheck('uncheck');
    });

    $('input').iCheck({
        checkboxClass: 'icheckbox_' + iCheck,
        radioClass: 'iradio_' + iCheck
    });
});

$(window).load(function () {
    $('.form-partition-wrapper:not(.with-errors)').each(function () {
        $(this).toggleBox();
    });
});