<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\db\Query;
use yii\db\Expression;
use yii\web\Response;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\AccessControl;
use vova07\imperavi\actions\GetAction;
use vova07\imperavi\actions\UploadAction;
use app\components\filters\AjaxFilter;
use app\models\AR\User;
use app\models\AR\Trigger;
use app\models\AR\Action;
use app\models\AR\Node;
use app\models\AR\Page;
use app\models\AR\Tag;
use app\models\AR\PageDraft;

class AjaxController extends Controller
{
    private $filesDir = 'files';
    private $imagesDir = 'images';
    private $uploadDir = 'src';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'ajax'   => [
                'class' => AjaxFilter::className(),
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                    [
                        'allow'   => true,
                        'roles'   => [User::ROLE_TESTER],
                        'actions' => [
                            'get-pages-list',
                            'get-actions-list',
                            'get-page-graph-data',
                            'get-node-graph-data',
                            'get-page-data',
                            'get-link-data',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $filesPath = '@webroot/' . $this->uploadDir . '/' . $this->filesDir;
        $imagesPath = '@webroot/' . $this->uploadDir . '/' . $this->imagesDir;

        $filesUrl = Yii::$app->homeUrl . $this->uploadDir . '/' . $this->filesDir;
        $imagesUrl = Yii::$app->homeUrl . $this->uploadDir . '/' . $this->imagesDir;

        return [
            'images-get'   => [
                'class' => GetAction::className(),
                'url'   => $imagesUrl,
                'path'  => $imagesPath,
                'type'  => GetAction::TYPE_IMAGES,
            ],
            'files-get'    => [
                'class' => GetAction::className(),
                'url'   => $filesUrl,
                'path'  => $filesPath,
                'type'  => GetAction::TYPE_FILES,
            ],
            'image-upload' => [
                'class' => UploadAction::className(),
                'url'   => $imagesUrl,
                'path'  => $imagesPath,
            ],
            'file-upload'  => [
                'class' => UploadAction::className(),
                'url'   => $filesUrl,
                'path'  => $filesPath,
            ],
        ];
    }

    /**
     * @param string $q
     * @param int $id
     * @return string
     */
    public function actionGetPagesList($q = null, $id = null)
    {
        return $this->getModelsList(Page::className(), $q, $id);
    }

    /**
     * @param string $q
     * @param int $id
     * @return string
     */
    public function actionGetActionsList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = [
            'results' => [
                'id'   => '',
                'text' => '',
            ],
        ];

        if (!is_null($q)) {
            $data = (new Query)
                ->select([
                    'id',
                    new Expression("concat(`text`, ' (', `from_id`, ' => ', `to_id`, ')') as text"),
                ])
                ->from(Action::tableName())
                ->where(['like', 'text', $q])
                ->orWhere(['like', 'id', $q])
                ->limit(10)
                ->createCommand()
                ->queryAll();

            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            /* @var $model \app\models\AR\Action */
            $model = Action::find($id);

            if ($model) {
                $out['results'] = [
                    'id'   => $id,
                    'text' => $model->text . ' (' . $model->from_id . ' => ' . $model->to_id . ')',
                ];
            }
        }

        return $out;
    }

    /**
     * @param array|null $pageTags
     * @param array|null $linkTags
     * @return mixed
     */
    public function actionGetPageGraphData(array $pageTags = null, array $linkTags = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = [
            'nodes' => [],
            'edges' => [],
        ];

        $pagesQuery = Page::find()
            ->with(['inputLinks', 'outputLinks'])
            ->joinWith(['tags']);
        if ($pageTags !== null) {
            $pagesQuery->where(['in', 'tag.id', $pageTags]);
            if (array_search(0, $pageTags) !== false) {
                $pagesQuery->orWhere(['tag.id' => null]);
            }
        }

        $linksQuery = Action::find()
            ->joinWith(['tags']);
        if ($linkTags !== null) {
            $linksQuery->where(['in', 'tag.id', $linkTags]);
            if (array_search(0, $linkTags) !== false) {
                $linksQuery->orWhere(['tag.id' => null]);
            }
        }

        foreach ($pagesQuery->all() as $key => $model) {
            /* @var $model \app\models\AR\Page */
            $current = [
                'id'          => $model->id,
                'label'       => $model->id . ': ' . $model->title,
                'value'       => (count($model->inputLinks) + count($model->outputLinks)),
                'borderWidth' => 1.35,
            ];

            if ($model->x !== null && $model->y !== null) {
                $current['x'] = $model->x;
                $current['y'] = $model->y;
            }

            $out['nodes'][$key] = $this->applyTags($current, $model->tags);
        }

        $linksCounter = [];
        foreach ($linksQuery->all() as $key => $model) {
            /* @var $model \app\models\AR\Action */
            $current = [
                'arrows' => 'to',
                'width'  => ($model->index - 0.35),
                'from'   => $model->from_id,
                'to'     => $model->to_id,
                'id'     => $model->id,
                'color'  => [
                    'inherit' => 'both',
                ],
            ];

            $out['edges'][$key] = $this->applyTags($current, $model->tags);

            $linksCounter[$model->from_id . '-' . $model->to_id][] = $key;
        }

        $inverseLinksCounter = [];
        foreach ($linksCounter as $key => $counter) {
            $count = count($counter);
            if ($count > 1) {
                foreach ($counter as $k => $v) {
                    $roundness = 1 / ($count) * $k;
                    $out['edges'][$v]['smooth'] = [
                        'type'      => $roundness >= 0.5 ? 'curvedCW' : 'curvedCCW',
                        'roundness' => $roundness < 0.5 ? $roundness : 1 - $roundness,
                    ];
                }
            }

            $chain = explode('-', $key);
            $inverseChain = $chain[1] . '-' . $chain[0];
            if (isset($linksCounter[$inverseChain]) && !isset($inverseLinksCounter[$key])) {
                $inverseLinksCounter[$inverseChain] = true;
                $inverseCount = count($linksCounter[$inverseChain]);

                foreach ($counter as $k => $v) {
                    $roundness = 1 / ($inverseCount) * $k;
                    $out['edges'][$v]['smooth']['type']      = 'cubicBezier';
                    $out['edges'][$v]['smooth']['roundness'] = (1 - $roundness) * 0.65;

                    if ($inverseCount != $count) {
                        break;
                    }
                }
            }
        }

        return $out;
    }

    /**
     * @param array|null $tags
     * @return mixed
     */
    public function actionGetNodeGraphData(array $tags = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = [
            'nodes' => [],
            'edges' => [],
        ];

        $query = Node::find()->joinWith(['tags']);

        if ($tags !== null) {
            $query->where(['in', 'tag.id', $tags]);
            if (array_search(0, $tags) !== false) {
                $query->orWhere(['tag.id' => null]);
            }
        }

        foreach ($query->all() as $key => $model) {
            /* @var $model \app\models\AR\Node */
            $current = [
                'id'          => $model->id,
                'label'       => $model->id . ': ' . $model->title,
                'borderWidth' => 1.35,
            ];

            if ($model->x !== null && $model->y !== null) {
                $current['x'] = $model->x;
                $current['y'] = $model->y;
            }

            $out['nodes'][$key] = $this->applyTags($current, $model->tags);
        }

        $relations = (new Query)
                ->select(['*'])
                ->from("{{%node_node}}")
                ->createCommand()
                ->queryAll();

        foreach ($relations as $edge) {
            $out['edges'][] = [
                'arrows' => 'to',
                'width'  => 0.65,
                'from'   => $edge['child_id'],
                'to'     => $edge['parent_id'],
                'color'  => [
                    'inherit' => 'both',
                ],
            ];
        }

        return $out;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionGetPageData($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (($model = Page::findOne($id)) === null) {
            throw new NotFoundHttpException('Not found.');
        }

        return [
            'title' => 'Страница #' . $id,
            'body'  => $this->renderPartial('@app/modules/admin/views/page/view', ['model' => $model]),
            'url'   => Url::to(['/admin/page/update', 'id' => $id]),
        ];
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionGetLinkData($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (($model = Action::findOne($id)) === null) {
            throw new NotFoundHttpException('Not found.');
        }

        return [
            'title' => 'Переход #' . $id,
            'body'  => $this->renderPartial('@app/modules/admin/views/action/view', ['model' => $model]),
            'url'   => '#',
        ];
    }

    /**
     * @param int $key
     * @return mixed
     */
    public function actionGetTriggerForm($key = 0)
    {
        return $this->renderAjax('@app/modules/admin/views/page/trigger/_form', [
            'model' => new Trigger(),
            'key'   => $key,
        ]);
    }

    /**
     * @param int $key
     * @return mixed
     */
    public function actionGetLinkForm($key = 0)
    {
        return $this->renderAjax('@app/modules/admin/views/page/link/_form', [
            'model' => new Action(),
            'key'   => $key,
        ]);
    }

    /**
     * @param int $id
     * @param int $x
     * @param int $y
     * @return mixed
     */
    public function actionSetPageCoordinates($id, $x, $y)
    {
        return $this->setCoordinates(Page::className(), $id, $x, $y);
    }

    /**
     * @param int $id
     * @param int $x
     * @param int $y
     * @return mixed
     */
    public function actionSetNodeCoordinates($id, $x, $y)
    {
        return $this->setCoordinates(Node::className(), $id, $x, $y);
    }
    
    /**
     * @param int $id
     * @return mixed
     */    
    public function actionAutosave($id) 
    {
        $model = PageDraft::findOne(['page_id' => $id]);
        if (!$model) {
            $model = new PageDraft();
            $model->page_id = $id;
        }        
        $model->text = Yii::$app->request->post('text');
        return $model->save();
    }

    /**
     * @param string $class
     * @param int $id
     * @param int $x
     * @param int $y
     * @return mixed
     */
    private function setCoordinates($class, $id, $x, $y)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (($model = $class::findOne($id)) === null) {
            throw new NotFoundHttpException('Not found.');
        }

        $model->x = $x;
        $model->y = $y;
        $model->detachBehavior('flashMessages');

        return $model->save(false);
    }

    /**
     * @param array $array
     * @param \app\models\AR\Tag[] $tags
     * @return array
     */
    private function applyTags($array, $tags)
    {
        $result = $array;

        foreach ($tags as $tag) {
            $chain = explode('.', $tag->property);
            $value = stripos($tag->property, 'dashes')
                ? ($tag->category == Tag::TYPE_PAGE ?  [5, 10] : (bool) $tag->value)
                : $tag->value;

            //Костыль, надо переделать на рекурсию
            switch (count($chain)) {
                case 1:
                    $result[$chain[0]] = $value;
                    break;
                case 2:
                    $result[$chain[0]][$chain[1]] = $value;
                    break;
            }

            if ($chain[0] == 'color') {
                if ($chain[1] != 'color') {
                    $result[$chain[0]]['highlight'][$chain[1]] = $value;
                } else {
                    $result[$chain[0]]['inherit']   = false;
                    $result[$chain[0]]['highlight'] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $class
     * @param string $q
     * @param int $id
     * @return string
     */
    private function getModelsList($class, $q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = [
            'results' => [
                'id'   => '',
                'text' => '',
            ],
        ];

        if (!is_null($q)) {
            $data = (new Query)
                ->select([
                    'id',
                    new Expression("concat(`id`, ': ', `title`) as text"),
                ])
                ->from($class::tableName())
                ->where(['like', 'title', $q])
                ->orWhere(['like', 'id', $q])
                ->limit(10)
                ->createCommand()
                ->queryAll();

            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            /* @var $model \app\models\AR\Node|\app\models\AR\Node */
            $model = $class::find($id);

            if ($model) {
                $out['results'] = [
                    'id'   => $id,
                    'text' => $model->id . ': ' . $model->title,
                ];
            }
        }

        return $out;
    }
}
