<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\search\AdminLogSearch;
use yii\web\Controller;
use app\modules\admin\controllers\traits\BehaviorsTrait;

class AdminLogController extends Controller
{
    use BehaviorsTrait;

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdminLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
