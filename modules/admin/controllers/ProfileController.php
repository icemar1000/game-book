<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\AR\User;
use app\modules\admin\models\forms\ProfileChangePasswordForm;
use yii\web\Controller;
use yii\filters\AccessControl;

class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionChangePassword()
    {
        $model = new ProfileChangePasswordForm;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->changePassword()) {
            Yii::$app->session->addFlash('success', '<i class="icon-info-sign"></i> Пароль изменен');
            return $this->redirect(['/admin/default']);
        }

        return $this->render('change-password', [
            'model' => $model,
        ]);
    }
}
