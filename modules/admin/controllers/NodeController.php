<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\AR\Node;
use app\modules\admin\models\search\NodeSearch;
use app\modules\admin\models\forms\TagMultipleForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\modules\admin\controllers\traits\BehaviorsTrait;

/**
 * NodeController implements the CRUD actions for Node model.
 */
class NodeController extends Controller
{
    use BehaviorsTrait;

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new TagMultipleForm(['entity' => 'node']);
        $searchModel = new NodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post()) && $model->apply()) {
            $model = new TagMultipleForm(['entity' => 'node']);
        }

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'model'        => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Node();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Node the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Node::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
