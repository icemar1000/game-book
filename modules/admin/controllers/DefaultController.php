<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\AR\User;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public $defaultAction = 'page';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_TESTER,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param array|null $pageTags
     * @param array|null $linkTags
     * @return string
     */
    public function actionPage(array $pageTags = null, array $linkTags = null)
    {
        return $this->render('page', [
            'pageTags' => $pageTags,
            'linkTags' => $linkTags,
        ]);
    }

    /**
     * @param array|null $tags
     * @return string
     */
    public function actionNode(array $tags = null)
    {
        return $this->render('node', [
            'tags' => $tags,
        ]);
    }
}
