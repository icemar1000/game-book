<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use app\models\AR\TextBuild;
use app\models\AR\User;

class TextBuildController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                        ],
                    ],
                    [
                        'allow'   => true,
                        'roles'   => [User::ROLE_TESTER],
                        'actions' => [
                            'index',
                            'view',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TextBuild::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TextBuild();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->consoleRunner->run('text-build/build ' . $model->id);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/game';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id)
    {
        $this->layout = '@app/views/layouts/game';
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        $model = $this->findModel($id);

        $options = [
            'mode'    => Pdf::MODE_UTF8,
            'content' => $this->render('view', ['model' => $model]),
            'options' => [
                'title'        => $model->title,
                'subject'      => 'PDF',
                'marginTop'    => 0,
                'marginBottom' => 0,
                'marginLeft'   => 0,
                'marginRight'  => 0,
            ],
        ];

        $cssFile = $model->getCssFile();

        if ($cssFile !== null) {
            $options['cssFile'] = $cssFile;
        }

        return (new Pdf($options))->render();
    }

    /**
     * @param integer $id
     * @return TextBuild the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TextBuild::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionJson(){
        $pages = \app\models\AR\Page::find()->with(['tags'])->asArray()->all();
        $triggers = \app\models\AR\Trigger::find()->asArray()->all();
        $objects = \app\models\AR\Object::find()->asArray()->all();
        $actions = \app\models\AR\Action::find()->with(['tags'])->asArray()->all();
        $achievements = \app\models\AR\Achievement::find()->asArray()->all();
        
        $_pages = [];
        foreach($pages as $key => $page) {
            $_pages[$key] = $page;
            $_pages[$key]['text'] = str_replace('/race/src/images', '/img/page', $page['text']);
        }        
        
        $result = [
            'pages' => $_pages,
            'triggers' => $triggers,
            'objects' => $objects,
            'actions' => $actions,
            'achievements' => $achievements,
        ];
        return Yii::$app->response->sendContentAsFile(json_encode($result), 'race.json');
    }
}
