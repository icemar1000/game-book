<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\forms\TagMultipleForm */
/* @var $tags array */
?>

<?php $form = ActiveForm::begin([
    'id'                     => 'TagMultipleForm',
    'enableClientValidation' => false,
]); ?>

<div class="row">
    <div class="col-xs-6">
        <?= $form->field($model, 'tag_ids_add')->widget(Select2::classname(), [
            'data'     => $tags,
            'options'  => [
                'multiple' => true,
                'prompt'   => '- Не добавлять -',
            ],
        ])->hint('Поиск по названию тега') ?>
    </div>

    <div class="col-xs-6">
        <?= $form->field($model, 'tag_ids_drop')->widget(Select2::classname(), [
            'data'     => $tags,
            'options'  => [
                'multiple' => true,
                'prompt'   => '- Не убирать -',
            ],
        ])->hint('Поиск по названию тега') ?>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton('Применить', [
        'class' => 'btn btn-success btn-flat btn-sm',
        'data'  => [
            'confirm' => 'Применить изменения?',
        ],
    ]) ?>
    <?= Html::a('Перейти вверх', '#top', [
        'class' => 'btn btn-sm bg-purple btn-flat to-anchor',
    ]) ?>
</div>

<div class="hiddenCheckboxes hidden"></div>

<?php ActiveForm::end(); ?>