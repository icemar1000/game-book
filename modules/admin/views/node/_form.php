<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\AR\Node;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Node */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="box box-primary">
        <a name="pageContent"></a>
        <div class="box-header with-border">
            <h3 class="box-title">Узел</h3>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'parent_ids')->widget(Select2::classname(), [
                'data'    => Node::getModelsList(),
                'options' => [
                    'multiple' => true,
                    'prompt'   => '- Нет -',
                ],
            ]) ?>

            <?= $form->field($model, 'children_ids')->widget(Select2::classname(), [
                'data'    => Node::getModelsList(),
                'options' => [
                    'multiple' => true,
                    'prompt'   => '- Нет -',
                ],
            ]) ?>

            <?= $form->field($model, 'tag_ids')->widget(Select2::classname(), [
                'data'    => Tag::getModelsListByCategory(Tag::TYPE_NODE),
                'options' => [
                    'multiple' => true,
                    'prompt'   => '- Без тегов -',
                ],
            ])->hint('Поиск по названию тега') ?>
        </div>
    </div>
    
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>
