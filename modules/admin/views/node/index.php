<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use kartik\select2\Select2;
use app\models\AR\Tag;
use app\models\AR\Node;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\NodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\admin\models\forms\TagMultipleForm */

$this->title = 'Узлы';
$this->params['breadcrumbs'][] = $this->title;

$tags = Tag::getModelsListByCategory(Tag::TYPE_NODE);
?>
<div class="page-index box box-primary">
    <a name="top"></a>
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>

        <div class="box-tools">
            <?= Html::a('Перейти вниз', '#bottom', ['class' => 'btn btn-sm bg-purple btn-flat to-anchor']) ?>
            <?= Html::a('Добавить узел', ['create'], ['class' => 'btn btn-success btn-flat btn-sm']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'format'        => 'html',
                    'filter'        => false,
                    'header'        => Html::checkbox('selectAllModels', false, [
                        'id'    => 'selectAllModels',
                        'class' => 'iCheck',
                    ]),
                    'headerOptions' => [
                        'width' => '30',
                    ],
                    'content'       => function ($model) {
                        $checkbox = Html::checkbox('TagMultipleForm[ids][]', false, [
                            'value' => $model->id,
                            'id'    => 'model-' . $model->id,
                            'class' => 'iCheck model-iCheck',
                        ]);
                        return Html::tag('div', $checkbox, [
                            'class' => 'modelCheckboxWrapper',
                        ]);
                    },
                ],
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width'     => '55',
                    ],
                ],
                'title',
                [
                    'attribute'     => 'parent_ids',
                    'filter'        => Select2::widget([
                        'model'     => $searchModel,
                        'attribute' => 'parent_ids',
                        'data'      => Node::getModelsList(),
                        'size'      => Select2::SMALL,
                        'options'   => [
                            'multiple' => true,
                            'prompt'   => '- Неважно -',
                        ],
                    ]),
                    'content'       => function ($model) {
                        /* @var $model app\models\AR\Node */
                        $models = $model->parents;
                        $return = "";

                        if ($models) {
                            foreach ($models as $node) {
                                /* @var $node app\models\AR\Node */
                                $return .= Html::tag('code', $node->id)
                                        . ' ' . $node->title
                                        . ' (' . $node->tagsNames . ')'
                                        .  Html::tag('br');
                            }
                        }

                        return $return;
                    },
                ],
                [
                    'attribute'     => 'children_ids',
                    'filter'        => Select2::widget([
                        'model'     => $searchModel,
                        'attribute' => 'children_ids',
                        'data'      => Node::getModelsList(),
                        'size'      => Select2::SMALL,
                        'options'   => [
                            'multiple' => true,
                            'prompt'   => '- Неважно -',
                        ],
                    ]),
                    'content'       => function ($model) {
                        /* @var $model app\models\AR\Node */
                        $models = $model->children;
                        $return = "";

                        if ($models) {
                            foreach ($models as $node) {
                                /* @var $node app\models\AR\Node */
                                $return .= Html::tag('code', $node->id)
                                        . ' ' . $node->title
                                        . ' (' . $node->tagsNames . ')'
                                        .  Html::tag('br');
                            }
                        }

                        return $return;
                    },
                ],
                [
                    'attribute'     => 'tag_ids',
                    'filter'        => Select2::widget([
                        'model'     => $searchModel,
                        'attribute' => 'tag_ids',
                        'data'      => Tag::getModelsListByCategory(Tag::TYPE_NODE),
                        'size'      => Select2::SMALL,
                        'options'   => [
                            'multiple' => true,
                            'prompt'   => '- Все -',
                        ],
                    ]),
                    'content'       => function ($model) {
                        /* @var $model app\models\AR\Page */
                        return $model->tagsNames;
                    },
                ],
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{update} {delete}',
                    'headerOptions' => [
                        'width'     => '55',
                    ],
                ],
            ],
        ]) ?>

        <hr>

        <a name="bottom"></a>

        <?= $this->render('@app/modules/admin/views/_shared/tag-multiple-form', [
            'model' => $model,
            'tags'  => $tags,
        ]) ?>
    </div>
</div>