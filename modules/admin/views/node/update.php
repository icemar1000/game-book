<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Node */

$this->title = 'Редактирование узла: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Узлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="node-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
