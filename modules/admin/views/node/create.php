<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Node */

$this->title = 'Новый узел';
$this->params['breadcrumbs'][] = ['label' => 'Узлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="node-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>