<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Page */

$this->title = 'Редактирование страницы: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="page-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
