<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use kartik\select2\Select2;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\admin\models\forms\TagMultipleForm */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;

$linkFilterWidget = [
    'model'         => $searchModel,
    'size'          => Select2::SMALL,
    'pluginOptions' => [
        'language' => [
            'errorLoading' => new JsExpression("
                function () {
                    return 'Загрузка...';
                }"
            ),
        ],
        'ajax' => [
            'url'      => Url::to(['/admin/ajax/get-pages-list']),
            'dataType' => 'json',
            'data'     => new JsExpression("
                function (params) {
                    return {
                        q: params.term
                    };
                }"
            ),
        ],
        'escapeMarkup'      => new JsExpression("
            function (markup) {
                return markup;
            }"
        ),
        'templateResult'    => new JsExpression("
            function (model) {
                return model.text;
            }"
        ),
        'templateSelection' => new JsExpression("
            function (model) {
                return model.text;
            }"
        ),
    ],
    'options' => [
        'multiple' => true,
        'prompt'   => '- Все -',
    ],
];

$tags = Tag::getModelsListByCategory(Tag::TYPE_PAGE);
?>
<div class="page-index box box-primary">
    <a name="top"></a>
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>

        <div class="box-tools">
            <?= Html::a('Перейти вниз', '#bottom', ['class' => 'btn btn-sm bg-purple btn-flat to-anchor']) ?>
            <?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success btn-flat btn-sm']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'format'        => 'html',
                    'filter'        => false,
                    'header'        => Html::checkbox('selectAllModels', false, [
                        'id'    => 'selectAllModels',
                        'class' => 'iCheck',
                    ]),
                    'headerOptions' => [
                        'width' => '30',
                    ],
                    'content'       => function ($model) {
                        $checkbox = Html::checkbox('TagMultipleForm[ids][]', false, [
                            'value' => $model->id,
                            'id'    => 'model-' . $model->id,
                            'class' => 'iCheck model-iCheck',
                        ]);
                        return Html::tag('div', $checkbox, [
                            'class' => 'modelCheckboxWrapper',
                        ]);
                    },
                ],
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width' => '55',
                    ],
                ],
                [
                    'attribute'     => 'number',
                    'headerOptions' => [
                        'width' => '85',
                    ],
                ],
                'title',
                [
                    'attribute' => 'tag_ids',
                    'filter'    => Select2::widget([
                        'model'     => $searchModel,
                        'attribute' => 'tag_ids',
                        'data'      => $tags,
                        'size'      => Select2::SMALL,
                        'options'   => [
                            'multiple' => true,
                            'prompt'   => '- Все -',
                        ],
                    ]),
                    'content'   => function ($model) {
                        /* @var $model \app\models\AR\Page */
                        return $model->tagsNames;
                    },
                ],
                [
                    'attribute'     => 'input_ids',
                    'headerOptions' => [
                        'width' => '350',
                    ],
                    'filter'        => Select2::widget(array_merge($linkFilterWidget, [
                        'attribute' => 'input_ids',
                    ])),
                    'content'       => function ($model) {
                        /* @var $model \app\models\AR\Page */
                        $output = [];
                        foreach ($model->inputLinks as $key => $link) {
                            if (!isset($output[$link->from_id])) {
                                $title = $link->from_id . ': ' . $link->from->title;
                                $output[$link->from_id] = Html::a($title, ['update', 'id' => $link->from_id], [
                                    'target' => '_blank',
                                ]);
                            }
                        }
                        return implode(', ', $output);
                    },
                ],
                [
                    'attribute'     => 'output_ids',
                    'headerOptions' => [
                        'width' => '350',
                    ],
                    'filter'        => Select2::widget(array_merge($linkFilterWidget, [
                        'attribute' => 'output_ids',
                    ])),
                    'content'       => function ($model) {
                        /* @var $model \app\models\AR\Page */
                        $output = [];
                        foreach ($model->outputLinks as $key => $link) {
                            if (!isset($output[$link->to_id])) {
                                $title = $link->to_id . ': ' . $link->to->title;
                                $output[$link->to_id] = Html::a($title, ['update', 'id' => $link->to_id], [
                                    'target' => '_blank',
                                ]);
                            }
                        }
                        return implode(', ', $output);
                    },
                ],
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{update} {delete}',
                    'headerOptions' => [
                        'width' => '55',
                    ],
                ],
            ],
        ]) ?>

        <hr>

        <a name="bottom"></a>

        <?= $this->render('@app/modules/admin/views/_shared/tag-multiple-form', [
            'model' => $model,
            'tags'  => $tags,
        ]) ?>
    </div>
</div>