<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\models\AR\User;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Page */
/* @var $anchor boolean */
?>

<div class="box box-info">
    <?php if (isset($anchor)): ?>
        <a name="inputLinks"></a>
    <?php endif; ?>
    <div class="box-header with-border">
        <h3 class="box-title">Переходы на эту страницу</h3>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $model->getInputLinksDataProvider(),
            'columns' => [
                [
                    'attribute' => 'from_id',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Action */
                        $text = $model->from_id . ': ' . $model->from->title;

                        if (Yii::$app->user->identity->role != User::ROLE_ADMIN) {
                            return $text;
                        }

                        return Html::a($text, ['/admin/page/update', 'id' => $model->from_id], ['target' => '_blank']);
                    },
                ],
                'text:raw',
                'index',
                [
                    'attribute' => 'condition',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Action */
                        return Html::tag('code', $model->condition);
                    },
                ],
                [
                    'attribute' => 'callback',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Action */
                        return Html::tag('code', $model->callback);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>