<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\models\AR\User;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Page */
?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Переходы c этой страницы</h3>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $model->getOutputLinksDataProvider(),
            'columns' => [
                [
                    'attribute' => 'to_id',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Action */
                        $text = $model->to_id . ': ' . $model->to->title;

                        if (Yii::$app->user->identity->role != User::ROLE_ADMIN) {
                            return $text;
                        }

                        return Html::a($text, ['/admin/page/update', 'id' => $model->to_id], ['target' => '_blank']);
                    },
                ],
                'text:raw',
                'index',
                [
                    'attribute' => 'condition',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Action */
                        return Html::tag('code', $model->condition);
                    },
                ],
                [
                    'attribute' => 'callback',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Action */
                        return Html::tag('code', $model->callback);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>