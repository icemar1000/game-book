<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use app\components\widgets\imperavi\Widget;
use kartik\select2\Select2;
use conquer\codemirror\CodemirrorWidget;
use app\components\widgets\ActiveForm;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Action */
/* @var $key int */
/* @var $form yii\widgets\ActiveForm */

$createPageLink = Html::a('Создать новую страницу', ['/admin/page/create'], ['target' => '_blank']);
$existsPageLink = $model->to_id ? Html::a('Открыть выбранную', [
    '/admin/page/update',
    'id' => $model->to_id,
], ['target' => '_blank']) : null;
?>

<div class="box box-default box-solid form-partition-wrapper <?= $model->hasErrors() ? 'with-errors' : '' ?>">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= is_object($model->to) ? $model->to_id . ': ' . $model->to->title : 'Новый переход' ?>
            <?php if ($model->text): ?>
                <small><?= strip_tags($model->text) ?></small>
            <?php endif; ?>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body form-partition" data-key="<?= $key ?>">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'cutFormTag' => true,
        ]); ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, "[$key]to_id")->widget(Select2::className(), [
            'initValueText' => is_object($model->to) ? $model->to_id . ': ' . $model->to->title : '',
            'pluginOptions' => [
                'language' => [
                    'errorLoading' => new JsExpression("
                        function () {
                            return 'Загрузка...';
                        }"
                    ),
                ],
                'ajax' => [
                    'url'      => Url::to(['/admin/ajax/get-pages-list']),
                    'dataType' => 'json',
                    'data'     => new JsExpression("
                        function (params) {
                            return {
                                q: params.term
                            };
                        }"
                    ),
                ],
                'escapeMarkup'      => new JsExpression("
                    function (markup) {
                        return markup;
                    }"
                ),
                'templateResult'    => new JsExpression("
                    function (page) {
                        return page.text;
                    }"
                ),
                'templateSelection' => new JsExpression("
                    function (page) {
                        return page.text;
                    }"
                ),
            ],
        ])->hint('Поиск по ID или названию. ' . $createPageLink . ($existsPageLink !== null ? ' / ' . $existsPageLink : "")) ?>

        <?= $form->field($model, "[$key]text")->widget(Widget::className()) ?>

        <?= $form->field($model, "[$key]tag_ids")->widget(Select2::classname(), [
            'data'     => Tag::getModelsListByCategory(Tag::TYPE_ACTION),
            'options'  => [
                'multiple' => true,
                'prompt'   => '- Без тегов -',
            ],
        ])->hint('Поиск по названию тега') ?>

        <?= $form->field($model, "[$key]condition")->widget(CodemirrorWidget::className()) ?>

        <?= $form->field($model, "[$key]callback")->widget(CodemirrorWidget::className()) ?>

        <?= $form->field($model, "[$key]index")->textInput(['type' => 'number']) ?>

        <?= $form->field($model, "[$key]idForSave")->hiddenInput()->label(false) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
