<?php

use app\components\widgets\ActiveForm;
use app\components\widgets\imperavi\Widget;
use conquer\codemirror\CodemirrorWidget;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Trigger */
/* @var $key int */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-default box-solid form-partition-wrapper <?= $model->hasErrors() ? 'with-errors' : '' ?>">
    <div class="box-header with-border">
        <h3 class="box-title">
            <?= $model->isNewRecord ? 'Новый триггер' : 'Триггер #' . $model->id ?>
            <?php if ($model->title): ?>
                <small><?= $model->title ?></small>
            <?php endif; ?>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body form-partition" data-key="<?= $key ?>">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'cutFormTag' => true,
        ]); ?>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, "[$key]title")->textInput() ?>

        <?= $form->field($model, "[$key]text")->widget(Widget::className()) ?>

        <?= $form->field($model, "[$key]condition")->widget(CodemirrorWidget::className()) ?>

        <?= $form->field($model, "[$key]callback")->widget(CodemirrorWidget::className()) ?>

        <?= $form->field($model, "[$key]priority")->textInput(['type' => 'number']) ?>
        
        <?= $form->field($model, "[$key]ones")->dropDownList([1 => 'Однократный',0 => 'Многократный']) ?>

        <?= $form->field($model, "[$key]idForSave")->hiddenInput()->label(false) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>