<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Page */
?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Триггеры</h3>
    </div>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $model->getTriggersDataProvider(),
            'columns' => [
                'title',
                'text:raw',
                'priority',
                [
                    'attribute' => 'condition',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Trigger */
                        return Html::tag('code', $model->condition);
                    },
                ],
                [
                    'attribute' => 'callback',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Trigger */
                        return Html::tag('code', $model->callback);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>