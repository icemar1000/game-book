<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use app\components\widgets\imperavi\Widget;
use kartik\select2\Select2;
use app\assets\AdminCMAsset;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Page */
/* @var $form yii\widgets\ActiveForm */

AdminCMAsset::register($this);

$enableInputLinksBox = !$model->isNewRecord && !empty($model->inputLinks);
?>

<div class="page-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'page-editor',
            'data-autosave' => Url::to(['/admin/ajax/autosave', 'id' => ($model->isNewRecord) ? time() : $model->id]),
        ],
    ]); ?>

    <div class="box box-primary">
        <a name="pageContent"></a>
        <div class="box-header with-border">
            <h3 class="box-title">Страница</h3>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?php if (0): ?>
                <?= $form->field($model, 'category')->dropDownList($model->getCategories()) ?>
            <?php endif; ?>

            <?= $form->field($model, 'text')->widget(Widget::className()) ?>

            <?= $form->field($model, 'tag_ids')->widget(Select2::classname(), [
                'data'     => Tag::getModelsListByCategory(Tag::TYPE_PAGE),
                'options'  => [
                    'multiple' => true,
                    'prompt'   => '- Без тегов -',
                ],
            ])->hint('Поиск по названию тега') ?>

            <?= $form->field($model, 'number')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="box box-success">
        <a name="outputLinks"></a>
        <div class="box-header with-border">
            <h3 class="box-title">Переходы на другие страницы</h3>
            <div class="box-tools pull-right">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-plus']), [
                    'class'       => 'btn btn-box-tool addRelation',
                    'data-url'    => Url::to(['/admin/ajax/get-link-form']),
                    'data-target' => '.pageLinks',
                ]) ?>
            </div>
        </div>
        <div class="box-body pageLinks">
            <?php foreach ($model->getRelated('outputLinks') as $key => $_model): ?>
                <?= $this->render('link/_form', [
                    'model' => $_model,
                    'key'   => $key,
                ]) ?>
            <?php endforeach; ?>
        </div>
        <div class="box-footer">
            <?= Html::button('Новый переход', [
                'class'       => 'btn btn-sm btn-primary btn-flat addRelation',
                'data-url'    => Url::to(['/admin/ajax/get-link-form']),
                'data-target' => '.pageLinks',
            ]) ?>
        </div>
    </div>

    <div class="box box-warning">
        <a name="triggers"></a>
        <div class="box-header with-border">
            <h3 class="box-title">Триггеры</h3>
            <div class="box-tools pull-right">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-plus']), [
                    'class'       => 'btn btn-box-tool addRelation',
                    'data-url'    => Url::to(['/admin/ajax/get-trigger-form']),
                    'data-target' => '.pageTriggers',
                ]) ?>
            </div>
        </div>
        <div class="box-body pageTriggers">
            <?php foreach ($model->getRelated('triggers') as $key => $_model): ?>
                <?= $this->render('trigger/_form', [
                    'model' => $_model,
                    'key'   => $key,
                ]) ?>
            <?php endforeach; ?>
        </div>
        <div class="box-footer">
            <?= Html::button('Новый триггер', [
                'class'       => 'btn btn-sm btn-primary btn-flat addRelation',
                'data-url'    => Url::to(['/admin/ajax/get-trigger-form']),
                'data-target' => '.pageTriggers',
            ]) ?>
        </div>
    </div>

    <?php if ($enableInputLinksBox): ?>
        <?= $this->render('link/_grid-input', [
            'model'  => $model,
            'anchor' => true,
        ]) ?>
    <?php endif; ?>

    <div class="box-fixed form-nav <?= $enableInputLinksBox ? '' : 'short' ?>">
        <div class="btn-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-sm bg-olive btn-flat']) ?>

            <?= Html::a('Контент', '#pageContent', ['class' => 'btn btn-sm bg-purple btn-flat to-anchor']) ?>
            <?= Html::a('Исходящие ссылки', '#outputLinks', ['class' => 'btn btn-sm bg-purple btn-flat to-anchor']) ?>
            <?= Html::a('Триггеры', '#triggers', ['class' => 'btn btn-sm bg-purple btn-flat to-anchor']) ?>

            <?php if ($enableInputLinksBox): ?>
                <?= Html::a('Входящие ссылки', '#inputLinks', ['class' => 'btn btn-sm bg-purple btn-flat to-anchor']) ?>
            <?php endif; ?>

            <?= Html::button('Новый переход', [
                'class'       => 'btn btn-sm bg-teal btn-flat addRelation',
                'data-url'    => Url::to(['/admin/ajax/get-link-form']),
                'data-target' => '.pageLinks',
            ]) ?>
            <?= Html::button('Новый триггер', [
                'class'       => 'btn btn-sm bg-teal btn-flat addRelation',
                'data-url'    => Url::to(['/admin/ajax/get-trigger-form']),
                'data-target' => '.pageTriggers',
            ]) ?>

            <?= Html::a('Новая страница', ['create'], [
                'class'  => 'btn btn-sm bg-navy btn-flat',
                'target' => '_blank',
            ]) ?>
        </div>
    </div>
    
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>
