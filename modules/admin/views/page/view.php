<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Page */
?>

<div class="page-view">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                Страница
            </h3>
        </div>
        <div class="box-body">
            <div>
                <label class="control-label">Название:</label>
                <?= $model->title ?>
            </div>

            <?php if ($model->tags): ?>
                <div>
                    <label class="control-label">Теги:</label>
                    <?= $model->tagsNames ?>
                </div>
            <?php endif; ?>

            <div>
                <label class="control-label">Текст:</label>
                <div>
                    <?= $model->text ?>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('link/_grid-output', [
        'model' => $model,
    ]) ?>

    <?= $this->render('link/_grid-input', [
        'model' => $model,
    ]) ?>

    <?= $this->render('trigger/_grid', [
        'model' => $model,
    ]) ?>
</div>
