<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Achievement */

$this->title = 'Изменить достижение: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Достижения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
