<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Achievement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Достижение</h3>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>