<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Достижения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-index">


<div class="tag-index box box-primary">
<div class="box-header with-border">
    <h3 class="box-title">Список</h3>

    <div class="box-tools">
        <?= Html::a('Добавить достижение', ['create'], ['class' => 'btn btn-success btn-flat btn-sm']) ?>
    </div>
</div>

<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name:ntext',
            'text:ntext',
            [
                'class'         => ActionColumn::className(),
                'template'      => '{update} {delete}',
                'headerOptions' => [
                    'width' => '55',
                ],
            ],
        ],
    ]); ?>
    </div>
</div>
