<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AR\User */

$this->title = 'Смена пароля: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-change-password">
    <div class="user-form">
        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Пользователь</h3>
            </div>

            <div class="box-body">
                <?= $form->field($model, 'password', [
                    'template' => '
                        {label}
                        <div class="input-group password-input">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default btn-flat generate" aria-label="Случайный пароль" title="Случайный пароль">
                                    <i class="fa fa-random"></i>
                                </button>
                            </div>
                            {input}
                        </div>
                        {error}{hint}',
                ])->passwordInput(['maxlength' => true])->hint('-', ['class' => 'password-hint']) ?>
            </div>
        </div>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
