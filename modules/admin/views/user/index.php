<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use kartik\select2\Select2;
use app\models\AR\User;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>

        <div class="box-tools">
            <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success btn-flat btn-sm']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => ['width' => '55'],
                ],
                'username',
                'email',
                [
                    'attribute' => 'role',
                    'filter'    => Select2::widget([
                        'model'     => $searchModel,
                        'attribute' => 'role',
                        'data'      => User::getRoles(),
                        'size'      => Select2::SMALL,
                        'options'   => [
                            'prompt'   => '- Все -',
                        ],
                    ]),
                    
                ],
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{update} {change-password} {delete}',
                    'headerOptions' => ['width' => '75'],
                    'buttons'       => [
                        'change-password' => function ($url) {
                            return Html::a('<span class="fa fa-key"></span>', $url, [
                                'title'      => 'Изменить пароль',
                                'aria-label' => 'Изменить пароль',
                            ]);
                        },
                    ],

                ],
            ],
        ]) ?>
    </div>
</div>