<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use yii\bootstrap\Dropdown;
use kartik\color\ColorInputAsset;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Теги';
$this->params['breadcrumbs'][] = $this->title;

ColorInputAsset::register($this);

$caret = Html::tag('b', '', ['class' => 'fa fa-caret-down']);
$dropdownBtnOptions = [
    'class'         => 'btn btn-success btn-flat btn-sm dropdown-toggle',
    'data-toggle'   => 'dropdown',
    'aria-expanded' => 'false',
];

$items = [];
foreach (Tag::getCategories() as $category => $label):
    foreach (Tag::getProperties($category) as $property => $label):
        $items[$category][] = [
            'label' => $label,
            'url'   => Url::to([
                'create',
                'category' => $category,
                'property' => $property,
            ]),
        ];
    endforeach;
endforeach;
?>

<div class="tag-index box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>

        <div class="box-tools">
            <span class="box-tools-label">
                <span class="text-green">Добавить тег:</span>
            </span>
            <?php foreach (Tag::getCategories() as $category => $label): ?>
                <div class="btn-group">
                    <?= Html::button($label, $dropdownBtnOptions) ?>
                    <?= Html::button($caret, $dropdownBtnOptions) ?>
                    <?= Dropdown::widget([
                        'items'   => $items[$category],
                        'options' => [
                            'class' => 'dropdown-menu dropdown-menu-right',
                        ],
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width' => '60',
                    ],
                ],
                'title',
                [
                    'attribute' => 'category',
                    'filter'    => Tag::getCategories(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Tag */
                        return $model->categoryName;
                    },
                ],
                [
                    'attribute' => 'property',
                    'filter'    => Tag::getProperties(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Tag */
                        return $model->propertyName;
                    },
                ],
                [
                    'attribute' => 'value',
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\Tag */
                        $vals = $model->getAvailablePropertyValues();

                        if ($vals == 'color') {
                            $inner = Html::tag('div', '', [
                                'class' => 'sp-preview-inner',
                                'style' => "background-color: {$model->value};",
                            ]);
                            $value = Html::tag('div', $inner, [
                                'class' => 'sp-preview',
                            ]);
                        } else {
                            $value = (isset($vals[$model->value]) ? $vals[$model->value] : '') . ' ';
                        }

                        return $value . Html::tag('code', $model->value);
                    },
                ],
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{update} {delete}',
                    'headerOptions' => [
                        'width' => '70',
                    ],
                ],
            ],
        ]) ?>
    </div>
</div>