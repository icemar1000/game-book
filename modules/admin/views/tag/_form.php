<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Tag */
/* @var $form yii\widgets\ActiveForm */

$values = $model->getAvailablePropertyValues();
?>

<div class="tag-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                Тег:
                (<?= $model->categoryName . ' | ' . $model->propertyName ?>)
            </h3>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?php if ($values == 'color'): ?>
                <?= $form->field($model, 'value')->widget(ColorInput::className(), [
                    'pluginOptions'=> ['preferredFormat' => 'rgb'],
                ]) ?>
            <?php else: ?>
                <?= $form->field($model, 'value')->dropDownList($values) ?>
            <?php endif; ?>
        </div>
    </div>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>
