<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Tag */

$this->title = 'Редактирование тега: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Теги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
