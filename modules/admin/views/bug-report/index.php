<?php

use yii\grid\GridView;
use app\components\grid\ActionColumn;
use yii\helpers\Html;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\models\AR\BugReport;
use app\models\AR\User;
use app\models\AR\Page;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\AdminLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баг-репорт';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tag-index box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width' => '60',
                    ],
                ],
                [
                    'attribute' => 'at',
                    'value'     => 'at',
                    'filter'    => DatePicker::widget([
                        'model'     => $searchModel,
                        'attribute' => 'at',
                    ]),
                    'format' => ['date', 'dd.MM.Y HH:mm'],
                ],
                [
                    'attribute' => 'user_id',
                    'filter'    => User::getUsersList(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\BugReport */
                        return $model->userName;
                    },
                ],
                [
                    'attribute' => 'page_id',
                    'filter'    => Select2::widget([
                        'model'     => $searchModel,
                        'attribute' => 'page_id',
                        'data'      => Page::getPagesList(),
                        'size'      => Select2::SMALL,
                        'options'   => ['prompt'   => '- Все -'],
                    ]),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\BugReport */
                        return \yii\bootstrap\Html::a($model->pageName , \yii\helpers\Url::to(['/admin/page/update','id'=>$model->page_id]));
                    },
                    'format' =>'raw'
                ],
                [
                    'attribute' => 'status',
                    'filter'    => BugReport::getStatuses(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\BugReport */
                        switch ($model->status) {
                            case BugReport::STATUS_ACTUAL:
                                $class = 'text-danger';
                                break;
                            case BugReport::STATUS_FIXED:
                                $class = 'text-success';
                                break;
                        }
                        return Html::tag('span', BugReport::getStatuses($model->status), [
                            'class' => $class,
                        ]);
                    },
                ],
                'message',
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{update} {delete}',
                    'headerOptions' => [
                        'width' => '70',
                    ],
                ],
            ],
        ]) ?>
    </div>
</div>