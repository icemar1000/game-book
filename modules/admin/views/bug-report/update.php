<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\AR\User;
use app\models\AR\Page;
use app\models\AR\BugReport;

/* @var $this yii\web\View */
/* @var $model app\models\AR\BugReport */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Редактирование баг-репорта: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Баг-репорты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bug-report-update">
    <div class="bug-report-form">
        <?php $form = ActiveForm::begin(); ?>

        <div class="box box-primary">
            <a name="pageContent"></a>
            <div class="box-header with-border">
                <h3 class="box-title">Баг-репорт</h3>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'page_id')->widget(Select2::classname(), [
                    'data' => Page::getPagesList(),
                ]) ?>

                <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                    'data' => User::getUsersList(),
                ]) ?>

                <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'status')->dropDownList(BugReport::getStatuses()) ?>
            </div>
        </div>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
