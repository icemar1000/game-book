<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminGraphAsset;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $pageTags array|null */
/* @var $linkTags array|null */

$this->title = 'Структура игры: страницы';

AdminGraphAsset::register($this);

$tagsAvailable = [
    'page'   => [0 => 'Без тегов'] + Tag::getModelsListByCategory(Tag::TYPE_PAGE),
    'action' => [0 => 'Без тегов'] + Tag::getModelsListByCategory(Tag::TYPE_ACTION),
];
$tagsSelected = [
    'page'   => !empty($pageTags) ? $pageTags : array_keys($tagsAvailable['page']),
    'action' => !empty($linkTags) ? $linkTags : array_keys($tagsAvailable['action']),
];
?>

<div class="modal modal-info fade"
     id="graph-tags-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="graphTagsLabel">
    <?php ActiveForm::begin([
        'method'  => 'get',
        'action'  => Url::to(['/admin/default']),
        'options' => [
            'class' => 'modal-dialog',
            'role'  => 'document',
        ],
    ]) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="graphTagsLabel">Теги</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="text-center">
                            <?= Html::label('Теги для страниц') ?>
                        </div>
                        <?= Html::checkbox('selectAllPageTags', true, [
                            'label' => 'Отметить все',
                            'id'    => 'selectAllPageTags',
                            'class' => 'iCheck',
                        ]) ?>
                        <?= Html::checkboxList('pageTags', $tagsSelected['page'], $tagsAvailable['page'], [
                            'separator'   => '<br>',
                            'itemOptions' => [
                                'class' => 'iCheck tag-page-iCheck',
                            ],
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-center">
                            <?= Html::label('Теги для переходов') ?>
                        </div>
                        <?= Html::checkbox('selectAllLinkTags', true, [
                            'label' => 'Отметить все',
                            'id'    => 'selectAllLinkTags',
                            'class' => 'iCheck',
                        ]) ?>
                        <?= Html::checkboxList('linkTags', $tagsSelected['action'], $tagsAvailable['action'], [
                            'separator'   => '<br>',
                            'itemOptions' => [
                                'class' => 'iCheck tag-link-iCheck',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-flat btn-outline pull-left" data-dismiss="modal">
                    Закрыть
                </button>
                <?= Html::a('Очистить', ['/admin/default'], [
                    'class'  => 'btn btn-sm btn-flat btn-outline',
                ]) ?>
                <?= Html::submitButton('Применить', ['class'  => 'btn btn-sm btn-flat btn-outline']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>

<span type="button" class="graph-tags" data-toggle="modal" data-target="#graph-tags-modal">
    <i class="fa fa-cog"></i>
</span>

<a href="<?= Url::to(['/admin/default/node']) ?>" class="graph-switch">
    <i class="fa fa-exchange"></i>
</a>

<?= Html::tag('div', '', [
    'id'                            => 'network',
    'data-url-set-node-coordinates' => Url::to(['/admin/ajax/set-page-coordinates']),
    'data-url-get-node'             => Url::to(['/admin/ajax/get-page-data']),
    'data-url-get-edge'             => Url::to(['/admin/ajax/get-link-data']),
    'data-url-get-graph'            => Url::to([
        '/admin/ajax/get-page-graph-data',
        'pageTags' => $pageTags,
        'linkTags' => $linkTags,
    ]),
]) ?>

<div class="modal modal-default fade"
     id="graph-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="graphModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="graphModalLabel"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm btn-flat pull-left" data-dismiss="modal">
                    Закрыть
                </button>
                <?php if (Yii::$app->user->can('admin')): ?>
                    <?= Html::a('Редактировать', '#', [
                        'class'  => 'btn btn-sm btn-flat btn-primary update-model',
                        'target' => '_blank',
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>