<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\AdminGraphAsset;
use app\models\AR\Tag;

/* @var $this yii\web\View */
/* @var $tags array|null */

$this->title = 'Структура игры: узлы';

AdminGraphAsset::register($this);

$tagsAvailable = [0 => 'Без тегов'] + Tag::getModelsListByCategory(Tag::TYPE_NODE);
$tagsSelected = !empty($tags) ? $tags : array_keys($tagsAvailable);
?>

<div class="modal modal-info fade"
     id="graph-tags-modal"
     tabindex="-1"
     role="dialog"
     aria-labelledby="graphTagsLabel">
    <?php ActiveForm::begin([
        'method'  => 'get',
        'action'  => Url::to(['/admin/default/node']),
        'options' => [
            'class' => 'modal-dialog',
            'role'  => 'document',
        ],
    ]) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="graphTagsLabel">Теги</h4>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <?= Html::label('Теги для узлов') ?>
                </div>
                <?= Html::checkbox('selectAllPageTags', true, [
                    'label' => 'Отметить все',
                    'id'    => 'selectAllPageTags',
                    'class' => 'iCheck',
                ]) ?>
                <?= Html::checkboxList('tags', $tagsSelected, $tagsAvailable, [
                    'separator'   => '<br>',
                    'itemOptions' => [
                        'class' => 'iCheck tag-page-iCheck',
                    ],
                ]) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-flat btn-outline pull-left" data-dismiss="modal">
                    Закрыть
                </button>
                <?= Html::a('Очистить', ['/admin/default/node'], [
                    'class'  => 'btn btn-sm btn-flat btn-outline',
                ]) ?>
                <?= Html::submitButton('Применить', ['class'  => 'btn btn-sm btn-flat btn-outline']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>

<span type="button" class="graph-tags" data-toggle="modal" data-target="#graph-tags-modal">
    <i class="fa fa-cog"></i>
</span>

<a href="<?= Url::to(['/admin/default/page']) ?>" class="graph-switch">
    <i class="fa fa-exchange"></i>
</a>

<?= Html::tag('div', '', [
    'id'                            => 'network',
    'data-url-set-node-coordinates' => Url::to(['/admin/ajax/set-node-coordinates']),
    'data-url-get-graph'            => Url::to([
        '/admin/ajax/get-node-graph-data',
        'tags' => $tags,
    ]),
]) ?>