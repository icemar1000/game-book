<?php

use yii\grid\GridView;
use kartik\date\DatePicker;
use app\models\AR\AdminLog;
use app\models\AR\User;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\AdminLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лог';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tag-index box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width' => '60',
                    ],
                ],
                [
                    'attribute' => 'at',
                    'value'     => 'at',
                    'filter'    => DatePicker::widget([
                        'model'     => $searchModel,
                        'attribute' => 'at',
                    ]),
                    'format' => ['date', 'dd.MM.Y HH:mm'],
                ],
                [
                    'attribute' => 'user_id',
                    'filter'    => User::getAdminsList(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\AdminLog */
                        return $model->userName;
                    },
                ],
                [
                    'attribute' => 'model',
                    'filter'    => AdminLog::getModelClasses(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\AdminLog */
                        return $model->modelName;
                    },
                ],
                [
                    'attribute' => 'action',
                    'filter'    => AdminLog::getActions(),
                    'content'   => function ($model) {
                        /* @var $model app\models\AR\AdminLog */
                        return $model->actionName;
                    },
                ],
                'model_id',
            ],
        ]) ?>
    </div>
</div>