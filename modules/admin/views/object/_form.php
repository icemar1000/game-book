<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Object */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tag-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Игровой объект</h3>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'category')->dropDownList($model->getCategories()) ?>
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>
