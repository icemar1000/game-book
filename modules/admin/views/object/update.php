<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Object */

$this->title = 'Редактирование объекта: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
