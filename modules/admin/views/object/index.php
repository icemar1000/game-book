<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use app\models\AR\Object;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\search\ObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Игровые объекты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tag-index box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>

        <div class="box-tools">
            <?= Html::a('Добавить объект', ['create'], ['class' => 'btn btn-success btn-flat btn-sm']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width' => '55',
                    ],
                ],
                [
                    'attribute' => 'category',
                    'filter'    => Object::getCategories(),
                ],
                'title',
                'text:ntext',
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{update} {delete}',
                    'headerOptions' => [
                        'width' => '55',
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
