<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\Object */

$this->title = 'Новый объект';
$this->params['breadcrumbs'][] = ['label' => 'Объекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
