<?php

use yii\helpers\Html;
use app\models\AR\User;

$isAdmin = Yii::$app->user->identity->role == User::ROLE_ADMIN;

/* @var $this yii\web\View */
/* @var $model app\models\AR\Action */
?>
<div class="action-view">
    <?php if (is_object($model->from)): ?>
        <div>
            <label class="control-label">Со страницы:</label>
            <?php $text = $model->from_id . ': ' . $model->from->title; ?>
            <?= !$isAdmin ? $text : Html::a($text, ['/admin/page/update', 'id' => $model->from_id], ['target' => '_blank']) ?>
        </div>
    <?php endif; ?>

    <div>
        <label class="control-label">На страницу:</label>
        <?php $text = $model->to_id . ': ' . $model->to->title; ?>
        <?= !$isAdmin ? $text : Html::a($text, ['/admin/page/update', 'id' => $model->to_id], ['target' => '_blank']) ?>
    </div>

    <div>
        <label class="control-label">Приоритет:</label>
        <?= $model->index ?>
    </div>

    <div>
        <label class="control-label">Условие:</label>
        <code><?= $model->condition ?></code>
    </div>

    <div>
        <label class="control-label">Скрипт:</label>
        <code><?= $model->callback ?></code>
    </div>

    <?php if ($model->tags): ?>
        <div>
            <label class="control-label">Теги:</label>
            <?= $model->tagsNames ?>
        </div>
    <?php endif; ?>

    <div>
        <label class="control-label">Текст:</label>
        <div>
            <?= $model->text ?>
        </div>
    </div>
</div>