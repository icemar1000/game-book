<?php

/* @var $this yii\web\View */
/* @var $model app\models\AR\TextBuild */

use yii\web\View;
use yii\debug\Module;
use app\models\AR\TextBuild;

$asset = $model->getOptionValue(TextBuild::OPTION_ASSET_BUNDLE);

if ($asset !== null) {
    $asset::register($this);
}

if (class_exists('yii\debug\Module')) {
    $this->off(View::EVENT_END_BODY, [Module::getInstance(), 'renderToolbar']);
}
?>

<?= $model->text ?>