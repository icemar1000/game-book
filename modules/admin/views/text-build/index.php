<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use kartik\color\ColorInputAsset;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сборки';
$this->params['breadcrumbs'][] = $this->title;

ColorInputAsset::register($this);
?>

<div class="tag-index box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Список</h3>

        <div class="box-tools">
            <?php if (Yii::$app->user->can('admin')): ?>
                <?= Html::a('Новая сборка', ['create'], ['class' => 'btn btn-success btn-flat btn-sm']) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'width' => '60',
                    ],
                ],
                'title',
                [
                    'attribute'     => 'status',
                    'headerOptions' => ['width' => '90'],
                    'content'       => function ($model) {
                        /* @var $model \app\models\AR\TextBuild */
                        return $model->statusName;
                    },
                ],
                [
                    'header'  => 'Настройки',
                    'content' => function ($model) {
                        /* @var $model \app\models\AR\TextBuild */
                        return Html::tag('code', $model->settings);
                    },
                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($model) {
                        /* @var $model \app\models\AR\TextBuild */
                        return $model->log ? $model->log->userName : '';
                    },
                ],
                [
                    'header'  => 'Время',
                    'content' => function ($model) {
                        /* @var $model \app\models\AR\TextBuild */
                        return $model->log ? date('d.m.Y H:i', $model->log->at) : '';
                    },
                ],
                [
                    'header'  => 'Печатных знаков',
                    'content' => function ($model) {
                        /* @var $model \app\models\AR\TextBuild */
                        $raw = strip_tags($model->text);
                        $str = preg_replace('/[^a-zA-Zа-яА-Я0-9]/ui', '', $raw);
                        return '~' . round(strlen($str) / 2);
                    },
                ],
                [
                    'class'         => ActionColumn::className(),
                    'template'      => '{view}', // '{view} {pdf}'
                    'headerOptions' => ['width' => '40'],
                    'buttonOptions' => ['target' => '_blank'],
                    'buttons'       => [
                        'pdf' => function ($url, $model, $key) {
                            $options = [
                                'title'      => 'PDF',
                                'aria-label' => 'PDF',
                                'target'     => '_blank',
                                'data-pjax'  => '0',
                            ];
                            $icon = Html::tag('i', '', ['class' => "fa fa-file-pdf-o"]);
                            return Html::a($icon, $url, $options);
                        },
                    ],
                ],
            ],
        ]) ?>
    </div>
</div>