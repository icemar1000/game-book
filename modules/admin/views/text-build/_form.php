<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\AR\Tag;
use app\models\AR\TextBuild;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\models\AR\TextBuild */
/* @var $form yii\widgets\ActiveForm */

$tags = [
    Tag::TYPE_PAGE   => Tag::getModelsListByCategory(Tag::TYPE_PAGE),
    Tag::TYPE_ACTION => Tag::getModelsListByCategory(Tag::TYPE_ACTION),
];
$tagsTypes = TextBuild::getSettingsTagsType();
$bundles = TextBuild::getAvailableAssetBundles();
?>

<div class="tag-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Сборка</h3>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?php foreach ($model->getAvailableSettings() as $option => $label): ?>
                <?php if (StringHelper::startsWith($option, 'tags_')): ?>
                    <div>
                        <?= Html::label($label) ?>
                    </div>

                    <?= Html::checkbox('select_' . $option, false, [
                        'data-select' => '.' . $option . '-iCheck',
                        'class'       => 'iCheck iCheckSelectAll',
                        'label'       => Html::tag('span', 'Отметить все', [
                            'class' => 'text-light-blue',
                        ]),
                    ]) ?>

                    <?= $form->field($model, 'settingsAsArray[' . $option . ']')->checkboxList($tags[$tagsTypes[$option]], [
                        'separator'   => '<br>',
                        'itemOptions' => [
                            'class' => 'iCheck ' . $option . '-iCheck',
                        ],
                    ])->label(false) ?>
                <?php endif; ?>

                <?php if (StringHelper::startsWith($option, 'asset_bundle')): ?>
                    <div>
                        <?= Html::label($label) ?>
                    </div>

                    <?= $form->field($model, 'settingsAsArray[' . $option . ']')->dropDownList($bundles, [
                        'prompt' => '- Нет -',
                    ])->label(false) ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>

    <?= Html::submitButton('Начать', ['class' => 'btn btn-success btn-block']) ?>

    <?php ActiveForm::end(); ?>
</div>
