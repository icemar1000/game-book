<?php
/* @var $this yii\web\View */
/* @var $model app\models\AR\TextBuild */

$this->title = 'Новая сборка';
$this->params['breadcrumbs'][] = ['label' => 'Сборки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-build-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
