<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\widgets\ModelErrorsWidget;

/* @var $this yii\web\View */
/* @var $model app\modules\models\forms\ProfileChangePasswordForm */

$this->title = 'Изменить пароль';
$this->params['breadcrumbs'][] = 'Изменить пароль';
?>
<div class="user-update">

    <div class="user-form">
        <?= ModelErrorsWidget::widget([
            'model' => $model,
        ]) ?>

        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'options' => [
                'enctype' => 'multipart/form-data',
                'class'   => 'box box-primary',
            ],
        ]); ?>

        <div class="box-header">
            <h3 class="box-title">Редактирование профиля</h3>
        </div>

        <div class="box-body">
            <?= $form->field($model, 'password', [
                'template' => '
                    {label}
                    <div class="input-group password-input">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default btn-flat generate" aria-label="Случайный пароль" title="Случайный пароль">
                                <i class="fa fa-random"></i>
                            </button>
                        </div>
                        {input}
                    </div>
                    {error}{hint}',
            ])->passwordInput(['maxlength' => true])->hint('-', ['class' => 'password-hint']) ?>
        </div>

        <div class="box-footer">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success btn-flat']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
