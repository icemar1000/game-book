<?php

use yii\helpers\Html;
use dmstr\web\AdminLteAsset;
use hiqdev\assets\icheck\iCheckAsset;
use app\assets\AdminAsset;

/* @var $this \yii\web\View */
/* @var $content string */

$title = 'Панель управления';

if ($this->title):
    $title .= ' - ' . $this->title;
endif;
?>
<?php if (Yii::$app->controller->action->id === 'login'): ?>
    <?= $this->render('main-login', ['content' => $content]) ?>
<?php else: ?>
    <?php
    AdminLteAsset::register($this);
    iCheckAsset::register($this);
    AdminAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= $title ?></title>
            <?php $this->head() ?>
        </head>
        <body class="hold-transition sidebar-mini skin-green sidebar-collapse">
            <?php $this->beginBody() ?>
            <div class="wrapper">

                <?= $this->render('_header.php', ['directoryAsset' => $directoryAsset]) ?>

                <?= $this->render('_left.php', ['directoryAsset' => $directoryAsset]) ?>

                <?= $this->render('_content.php', [
                    'content' => $content,
                    'directoryAsset' => $directoryAsset,
                ]) ?>

            </div>
            <?php $this->endBody() ?>
        </body>
    </html>
    <?php $this->endPage() ?>
<?php endif; ?>