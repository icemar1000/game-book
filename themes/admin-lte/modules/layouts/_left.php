<?php

use dmstr\widgets\Menu;
use app\components\helpers\TextCounter;

/* @var $this yii\web\View */

$controller = Yii::$app->controller->id;
$action     = Yii::$app->controller->action->id;

$checkRoute = function ($c, $a = null) use ($controller, $action) {
    $result = $c == $controller;
    if ($a !== null) {
        $result &= $a == $action;
    }
    return $result;
};
?>

<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'items' => [
                [
                    'label'   => 'Печатных знаков: ~' . TextCounter::getCharsCount(),
                    'options' => [
                        'class' => 'header text-center',
                    ],
                ],
                [
                    'label'  => 'Структура (страницы)',
                    'icon'   => 'sitemap',
                    'url'    => ["/admin/default/page"],
                    'active' => $checkRoute('default', 'page'),
                ],
                [
                    'label'  => 'Структура (узлы)',
                    'icon'   => 'map',
                    'url'    => ["/admin/default/node"],
                    'active' => $checkRoute('default', 'node'),
                ],
                [
                    'label'   => 'Узлы',
                    'icon'    => 'map-signs',
                    'url'     => ["/admin/node"],
                    'active'  => $checkRoute('node'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Страницы',
                    'icon'    => 'file-text-o',
                    'url'     => ["/admin/page"],
                    'active'  => $checkRoute('page'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Теги',
                    'icon'    => 'bookmark',
                    'url'     => ["/admin/tag"],
                    'active'  => $checkRoute('tag'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Игровые объекты',
                    'icon'    => 'cube',
                    'url'     => ["/admin/object"],
                    'active'  => $checkRoute('object'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Достижения',
                    'icon'    => 'star',
                    'url'     => ["/admin/achievement"],
                    'active'  => $checkRoute('achievement'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Лог',
                    'icon'    => 'history',
                    'url'     => ["/admin/admin-log"],
                    'active'  => $checkRoute('admin-log'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Баг-репорты',
                    'icon'    => 'bug',
                    'url'     => ["/admin/bug-report"],
                    'active'  => $checkRoute('bug-report'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Пользователи',
                    'icon'    => 'users',
                    'url'     => ["/admin/user"],
                    'active'  => $checkRoute('user'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
                [
                    'label'   => 'Экспорт в JSON',
                    'icon'    => 'code-fork',
                    'url'     => ["/admin/text-build/json"],
                    'active'  => $checkRoute('user'),
                    'visible' => Yii::$app->user->can('admin'),
                ],
//                [
//                    'label' => 'Действия',
//                    'icon'  => 'code-fork',
//                    'url'   => ["/admin/action"],
//                ],
//                [
//                    'label' => 'Триггеры',
//                    'icon'  => 'code',
//                    'url'   => ["/admin/trigger"],
//                ],
//                [
//                    'label' => 'Объекты',
//                    'icon'  => 'cube',
//                    'url'   => ["/admin/object"],
//                ],
            ],
        ]) ?>
    </section>
</aside>
