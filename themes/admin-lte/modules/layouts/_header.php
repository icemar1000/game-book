<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\AR\BugReport;

/* @var $this \yii\web\View */
/* @var $content string */

$actualBugs = BugReport::getActiveBugs();

$logo = '<span class="logo-mini">RB</span>'
        . '<span class="logo-lg">RogueBook</span>';
?>

<header class="main-header">

    <?= Html::a($logo, Yii::$app->homeUrl, [
        'class' => 'logo',
        'target' => '_blank',
    ]) ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?=Url::to(['/admin/bug-report'])?>">
                        <span class="hidden-xs">
                            Активных баг-репортов:
                            <span class="label label-<?= (bool) $actualBugs ? 'danger' : 'success' ?>">
                                <?= BugReport::getActiveBugs() ?>
                            </span>
                        </span>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Изменить пароль', ['/admin/profile/change-password'], [
                                    'class' => 'btn btn-warning btn-flat',
                                ]) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a('Выйти', ['/site/logout'], [
                                    'data-method' => 'post',
                                    'class' => 'btn btn-default btn-flat',
                                ]) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
