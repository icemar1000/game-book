<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Inflector;
use dmstr\widgets\Alert;
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])): ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php else: ?>
            <h1>
                <?php if ($this->title !== null): ?>
                    <?= Html::encode($this->title) ?>
                <?php else: ?>
                    <?= Inflector::camel2words(Inflector::id2camel($this->context->module->id)) ?>
                    <?= ($this->context->module->id !== Yii::$app->id)
                        ? '<small>Module</small>'
                        : '' ?>
                <?php endif; ?>
            </h1>
        <?php endif; ?>

        <?= Breadcrumbs::widget([
            'homeLink' => ['label' => 'Главная', 'url' => ['/admin/default']],
            'links' => isset($this->params['breadcrumbs'])
                ? $this->params['breadcrumbs']
                : [],
        ]) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <strong>Powered by Yii</strong>
</footer>