<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class ToolsController extends Controller
{
    public function actionBackup()
    {
        /** @var \demi\backup\Component $backup */
        $backup = Yii::$app->backup;
        $file = $backup->create();
        $this->stdout('Backup file created: ' . $file . PHP_EOL, Console::FG_GREEN);
    }
}