<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\base\Exception;
use app\models\AR\TextBuild;
use app\models\AR\Page;
use app\models\AR\Tag;

class TextBuildController extends Controller
{
    public function actionBuild($id)
    {
        $model = TextBuild::findOne($id);

        if ($model === null) {
            return;
        }

        $query = Page::find()
            ->select([
                'page.*',
                new Expression('if(page.number is null, 1, 0) as number_flag'),
            ])
            ->joinWith([
                'outputLinks',
                'outputLinks.tags',
                'tags' => function ($query) {
                    /* @var $query \yii\db\ActiveQuery */
                    $query->from(['_tag' => Tag::tableName()]);
                },
            ])
            ->orderBy([
                'number_flag' => SORT_ASC,
                'page.number' => SORT_ASC,
                'page.id'     => SORT_ASC,
            ]);

        if ($model->getOptionValue(TextBuild::OPTION_TAGS_ALLOWED) !== null) {
            $query->where([
                '_tag.id' => $model->getOptionValue(TextBuild::OPTION_TAGS_ALLOWED),
            ]);
        }

        $pages = $query->all();
        $count = count($pages);
        $range = array_flip(range(1, $count));
        $links = [];

        $filter = function ($var) {
            return is_object($var);
        };

        if ($count == 0) {
            $model->status = TextBuild::STATUS_NO_CONTENT;
            $model->save();
            return;
        }

        //Мутная сортировка страниц
        foreach ($pages as $key => $page) {
            if (!is_int($page->number)) {
                break;
            }

            $range[$page->number] = $page;
            $links[$page->id] = $page->number;
            unset($pages[$key]);
        }

        shuffle($pages);

        foreach ($pages as $key => $page) {
            //Стартуем с цифры 1
            $i = $key == 0 ? 1 : $key;

            while (isset($range[$i]) && is_object($range[$i])) {
                $i++;
            }

            $range[$i] = $page;
            $links[$page->id] = $i;
        }

        $status = TextBuild::STATUS_COMPLETE;

        try {
            $model->text = $this->renderPartial('@app/components/views/build/text', [
                'pages' => array_filter($range, $filter),
                'links' => $links,
                'build' => $model,
            ]);
        } catch (Exception $e) {
            $status = TextBuild::STATUS_FAILED;
            $model->text = $e->getMessage() . $model->text;
        }

        $model->status = $status;

        $model->detachBehavior('flashMessages');
        $model->detachBehavior('adminLog');

        return $model->save();
    }
}