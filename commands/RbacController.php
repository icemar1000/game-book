<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $permissionTester = $auth->createPermission('tester');
        $permissionAdmin = $auth->createPermission('admin');

        $auth->add($permissionTester);
        $auth->add($permissionAdmin);

        $rule = new UserRoleRule();

        $auth->add($rule);

        $tester = $auth->createRole('tester');
        $tester->ruleName = $rule->name;

        $auth->add($tester);

        $admin = $auth->createRole('admin');
        $admin->ruleName = $rule->name;

        $auth->add($admin);

        $auth->addChild($admin, $tester);
    }
}