<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->context->layout = 'blank';
$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe', [
                    'template' => '<div class="form-group">'
                                . '    <label class="control-label">{label}</label>'
                                . '    <br>'
                                . '    {input}'
                                . '</div>',
                ])->checkbox(['class' => 'iCheck'], false) ?>

                <div class="form-group">
                    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <?= Html::a('Не помню пароль', ['site/request-password-reset'], ['class' => 'btn btn-link']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>