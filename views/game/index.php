<?php

use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $game app\models\AR\Game */

$this->title = 'SpaceRace';
?>

<div class="book">
    <h1 class="text-center">SpaceRace</h1>
    <?= Html::a('Новая игра', Url::to(['game/new']), [
        'class' => 'new-game',
    ]) ?>
    <?= Html::a('[Баг-репорт]', '#', [
        'class' => 'new-game',
        'style' => 'margin-left: 120px;',
        'data-toggle' => 'modal',
        'data-target' => '#bug-report-modal',
    ]) ?>
    <div class="page">
        <?= $page_render ?>
    </div>

</div>



