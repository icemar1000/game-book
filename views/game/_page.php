<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use app\models\AR\User;
use app\models\AR\Trigger;
use app\models\AR\GameObject;
use app\models\AR\BugReport;
use app\components\widgets\ActiveForm;
use dmstr\widgets\Alert;

/* @var $this yii\web\View */
/* @var $page app\models\AR\Page */

if (Yii::$app->user->identity->game) {
    $data = Yii::$app->user->identity->game->getData();
}

$bugReportModel = new BugReport;

$show_objects = true;

if ($page->id == 455) {
    $func = Trigger::checkObject();
    if ($func("Возврат в прошлое")) {
        $money->value = 0;
        $show_objects = false;
    }
}
?>

<?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN): ?>
    <div style="margin-bottom: -10px; margin-top: 15px;">
        <?= Html::a('Редактировать страницу', ['/admin/page/update', 'id' => $page->id], [
            'target' => '_blank',
            'class' => 'btn btn-primary btn-xs',
        ]) ?>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-9 col-md-8 col-sm-7">
        <div class="page-text">
            <div class="page-text-bg">
                <?= Alert::widget()?>
                <?= $page->text ?>
            </div>
        </div>
        <ul class="page-links">
            <?php foreach ($links as $link): ?>
                <?php
                $linkText = ($link['active'])
                    ? Html::a($link['text'], '#', ['data-url' => $link['url']])
                    : $link['text']
                ?>
                <li>
                    <?= Html::tag('div', $linkText, ['class' => 'page-link']) ?>
                </li>
            <?php endforeach;?>
        </ul>
    </div>

    <div class="col-lg-3 col-md-4 col-sm-5" style="color: rgb(159, 154, 255);">
        <h3>Деньги</h3>
        <?= $money->value ?> CR

        <?php if ($game->items && $show_objects): ?>
            <hr>
            <h3>Предметы</h3>
            <?php foreach ($game->items as $object): ?>
                <?= $object->text ?>
                <br>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($game->upgrades && $show_objects): ?>
            <hr>
            <h3>Улучшения</h3>
            <?php foreach ($game->upgrades as $object): ?>
                <?= $object->text ?>
                <br>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($game->keywords && $show_objects): ?>
            <hr>
            <h3>Ключевое слово</h3>
            <?php foreach ($game->keywords as $object): ?>
                <?= $object->text ?>
                <br>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($game->showMarkers && $show_objects): ?>
            <hr>
            <?php $markerColors = [
                'Черви' =>'#e01616',
                'Буби' =>'#ff9f0f',
                'Трефы' =>'#27a000',
                'Пики' =>'#2801ff',
                
            ];?>
            <?php foreach ($game->showMarkers as $object): ?>
            <span class="">
                <?= $object->object->text ?>                    
            </span>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="<?= $object->value?>" aria-valuemin="0" aria-valuemax="20" style="background-color:<?=$markerColors[$object->object->text]?>; width: <?= $object->value*5?>%;">
                </div>
            </div>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if ($game->hand && $show_objects): ?>
            <hr>
            <h3>Карты</h3>
            <?php $suit = ''; ?>
            <?php foreach ($game->hand as $card): ?>
                <?php if ($suit != $card->object->text): ?>
                    <?php if ($suit != ''): ?>
                        </ul>
                    <?php endif; ?>
                <?php $suit = $card->object->text; ?>
                <h4><?= $card->object->text ?></h4>
                <ul>
                <?php endif; ?>
                <li>
                    <?= GameObject::getCardValueLabels()[$card->value] ?>
                </li>
            <?php endforeach; ?>
            </ul>
        <?php endif; ?>

        <br>
    </div>
</div>

<div class="modal fade" id="bug-report-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <?php $form = ActiveForm::begin([
            'id' => 'bug-report-form',
            'action' => Url::to(['/bug-report/ajax-create']),
            'options' => ['class' => 'modal-content'],
        ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Баг-репорт</h4>
            </div>
            <div class="modal-body">
                <?= $form->field($bugReportModel, 'message')->textarea(['rows' => 6]) ?>
                <?= $form->field($bugReportModel, 'page_id')->hiddenInput(['value' => $page->id])->label(false) ?>
                <?= $form->field($bugReportModel, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>
            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
