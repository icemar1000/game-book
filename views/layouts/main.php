<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);

$this->title = 'RogueBook';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <div class="ribbon">
                <div class="ribbon-stitches-top"></div>
                <strong class="ribbon-content">
                    <h1>A Pure CSS Ribbon</h1>
                </strong>
                <div class="ribbon-stitches-bottom"></div>
            </div>

            <div class="container">
                <?= $content ?>
            </div>
        </div>

        <?php if (0): ?>
        <footer class="footer">
            <div class="container">
                <p class="text-center"><?= Yii::powered() ?></p>
            </div>
        </footer>
        <?php endif; ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
