<?php

namespace app\rbac;

use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use app\models\AR\User;

class UserRoleRule extends Rule
{
    public $name = 'userRole';

    public function execute($_user, $item, $params)
    {
        $user = ArrayHelper::getValue($params, 'user', User::findOne($_user));

        if ($user) {
            $role = $user->role;

            if ($item->name === 'admin') {
                return $role == User::ROLE_ADMIN;
            } elseif ($item->name === 'tester') {
                return $role == User::ROLE_ADMIN
                        || $role == User::ROLE_TESTER;
            } elseif ($item->name === 'player') {
                return $role == User::ROLE_ADMIN
                        || $role == User::ROLE_TESTER
                        || $role == User::ROLE_PLAYER;
            }
        }

        return false;
    }
}