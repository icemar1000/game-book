<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\db\Expression;
use app\models\AR\User;
use app\models\AR\Game;
use app\models\AR\Page;
use app\models\AR\Action;
use app\models\AR\Object;
use app\models\AR\Trigger;
use app\models\AR\GameObject;

class GameController extends Controller
{

    public $layout = 'game';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_TESTER,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }

    /**
     * Main game page
     *
     * @return string
     */
    public function actionIndex()
    {
        $game = Yii::$app->user->identity->game;

        if (!$game) {
            $game = new Game();
            $game->save();
        }

        return $this->render('index', [
            'game' => $game,
            'page_render' => $this->show(),
        ]);
    }

    public function actionNew($quantum = false)
    {
        if (Yii::$app->user->identity->game) {
            Yii::$app->user->identity->game->delete();
        }

        if ($quantum) {
            $game = new Game();
            $game->initQuantum();
            $game->save();
        }
        
        $this->redirect(['index']);
    }
    
    public function actionDrop($title)
    {
        $game = Game::getGameIdentity();
        $data = $game->getData();
        $function = Trigger::dropObject();
        $function($title);
        unset($data['only']);
        unset($data['links']);
        $game->setData($data);
        $game->save();
        return $this->show();
    }    
    
    public function actionBuy($item, $price, $trigger = null)
    {
        if ($trigger) {
            $trigger_model = new Trigger();
            $trigger_model->callback = $trigger;
            $trigger_model->exec();
        }

        $item = Object::byID($item);

        $game = Game::getGameIdentity();
        $game->link('objects', $item);

        $money = $game->money();
        $money->value -= $price;
        $money->save();

        return $this->actionMove($game->page_id);
    }

    public function actionDraw($suit, $quantity, $value = null)
    {
        $game = Game::getGameIdentity();
        $data = $game->getData();
        
        for ($i = 1; $i <= $quantity; $i++) {
            Game::getGameIdentity()->drawCard($suit, $value);
        }
        unset($data['drawFromDiscard']);
        unset($data['links']);
        unset($data['only']);
        $game->setData($data);
        $game->save();
        return $this->show();
    }

    public function actionGetkeyword($title)
    {
        $game = Game::getGameIdentity();
        $data = $game->getData();
        
        $item = Object::byTitle($title); 
        $game->link('objects', $item);
        
        unset($data['getKeyword']);
        unset($data['links']);
        unset($data['only']);
        
        $game->setData($data);
        $game->save();
        return $this->show();
    }

    public function actionSell($item, $price)
    {
        $game = Game::getGameIdentity();

        $money = $game->money();
        $money->value += $price;
        $money->save();

        $item_model = GameObject::find()
            ->where(['id' => $item])
            ->one();

        if (!$item_model) {
            $item_model = GameObject::find()
                ->where(['object_id' => $item])
                ->andWhere(['game_id' => $game->id])
                ->one();
        }

        if ($item_model->object->category == Object::$CATEGORY_CARD) {
            $game->discardCard($item_model->object->title, $item_model->value);
        } else {
            $item_model->delete();
        }

        return $this->actionMove($game->page_id);
    }
    
    public function actionDiscard($suit, $value, $drawAfter = false)
    {
        Game::getGameIdentity()->discardCard($suit, $value);
        $game = Game::getGameIdentity();
        $data = $game->getData();
        
        foreach ($data['links'] as $key => $link) {
            if ($link['url'] == Url::to([
                'game/discard',
                'value' => $value,
                'suit' => $suit,
                'drawAfter' => $drawAfter,
            ])) {
                unset($data['links'][$key]);
            }
        }
        
        $data['discard']--;

        if ($data['discard'] == 0) {
            unset($data['discard']);
            unset($data['links']);
            unset($data['only']);
        } else {
            if (count($data['links']) == 0) {
                unset($data['discard']);
            }

            $data['only'] = true;
            $data['links'][] = [
                'url' => Url::to([
                    'game/move',
                    'pageId' => 52,
                ]),
                'text' => 'Поражение',
                'active' => true,
            ];
        }
        
        if ($drawAfter) {
            $suits = Object::getCardSuits();

            foreach ($suits as $suit) {
                $cards = GameObject::find()
                    ->bySuit($suit)
                    ->byGame($game->id)
                    ->byPlace(GameObject::$CARD_PLACE_DECK)
                    ->all();

                if (count($cards) >= 2) {
                    $data['links'][] = [
                        'url' => Url::to([
                            'game/draw',
                            'suit' => $suit,
                            'quantity' => 2
                        ]),
                        'text' => 'Взять 2 карты масти ' . $cards[0]->object->text,
                        'active' => true,
                    ];
                }
            }

            if (isset($data['links'])) {
                $data['only'] = true;
            }
            
            $game->setData($data);
            $game->save();
        }

        $game->setData($data);
        $game->save();
        
        return $this->show();
    }

    /**
     * @return Page
     */
    public function resetQuantum()
    {
        return Page::find()
            ->joinWith('tags')
            ->where(['like', 'tag.title', 'Квантовый: первый круг'])
            ->orderBy(new Expression('rand()'))
            ->one();
    }
    
    public function show()
    {
        $game = Game::getGameIdentity();

        if ($game->page->triggersMulti) {
            foreach ($game->page->triggersMulti as $trigger) {
                if (($trigger->condition && $trigger->check()) || !$trigger->condition) {
                    $trigger->exec();
                }
            }
        }

        $game = Game::getGameIdentity();
        $data = $game->getData();

//        if (stristr($game->page->tagsNames, 'Квантовый: точка входа')) {
//            $startPage = Page::find()
//                ->joinWith('tags')
//                ->where(['like', 'tag.title', 'Квантовый: первый круг'])
//                ->orderBy(new Expression('rand()'))
//                ->one();
//
//            $data = (array) $game->getData();
//            unset($data['quantum']);
//
//            $data['links'][] = [
//                'url' => Url::to([
//                    'game/move',
//                    'pageId' => $startPage->id,
//                ]),
//                'text' => 'Точка отсчёта - начало Гонки',
//                'active' => true,
//            ];
//
//            $game->setData($data);
//            $game->save();
//        }

//        if (stristr($game->page->tagsNames, 'Квантовый: веха') && stristr($game->page->tagsNames, 'Квантовый: первый круг')) {
//            $data = Yii::$app->user->identity->game->getData();
//            $data['quantum']['visited'][] = $game->page->id;
//
//            $game->setData($data);
//            $game->save();
//        }
//
//        if (stristr($game->page->tagsNames, 'Квантовый: далее')) {
//            $data = $game->getData();
//
//            $nextPage = Page::find()
//                ->joinWith('tags')
//                ->where(['like', 'tag.title', 'Квантовый: первый круг'])
//                ->andWhere(['not in', 'page.id', $data['quantum']['visited']])
//                ->orderBy(new Expression('rand()'))
//                ->one();
//
//            if (!$nextPage && stristr($game->page->tagsNames, 'Квантовый: рекурсия')) {
//                $nextPage = $this->resetQuantum();
//                $data = Yii::$app->user->identity->game->getData();
//            }
//
//            if (!$nextPage) {
//                $nextPage = Page::find()
//                    ->joinWith('tags')
//                    ->where([
//                        'or',
//                        ['like', 'tag.title', 'Квантовый: второй круг'],
//                        ['like', 'tag.title', 'Квантовый: третий круг'],
//                    ])
//                    ->andWhere(['not in', 'page.id', $data['quantum']['visited']])
//                    ->orderBy(new Expression('rand()'))
//                    ->one();
//
//                $data['quantum']['visited'] = [];
//            }

//            $data['links'][] = [
//                'url' => Url::to([
//                    'game/move',
//                    'pageId' => $nextPage->id,
//                ]),
//                'text' => '...',
//                'active' => true,
//            ];
//
//            $game->setData($data);
//        }

        $game->save();
        $data = $game->getData();

        $dynamic_links = isset($data['links']) ? $data['links'] : [];
        $static_links = [];

        if (!isset($data['only'])) {
            foreach ($game->page->outputLinks as $key => $link) {
                $activeLink = false;

                foreach (Game::$actualTags as $tag) {
                    if (in_array($tag, $link->to->tag_ids)) {
                        $activeLink = true;
                    }
                }

                if (isset($activeLinks[$key])) {
                    $activeLink = $activeLinks[$key];
                }

                $condition = ($link->condition) ? $link->check() : true;

                if ($condition !== null) {
                    $static_links[] = [
                        'url' => Url::to([
                            'game/move',
                            'pageId' => stristr($link->tagsNames, 'Квантовый: рекурсия')
                                ? $this->resetQuantum()->id
                                : $link->to_id,
                            'linkId' => $link->id,
                        ]),
                        'text' => $link->text,
                        'active' => $condition && $activeLink,
                    ];

                    $data = Yii::$app->user->identity->game->getData();
                }
            }

            shuffle($static_links);

            foreach ($static_links as $key => $link) {
                if (!$link['active']) {
                    array_push($static_links, $link);
                    unset($static_links[$key]);
                }
            }
        }

        $links = array_merge($dynamic_links, $static_links);
        
        //очистка динамических линков после вывода
        if (    !isset($data['getKeyword'])
             && !isset($data['drawFromDiscard']) 
             && (!isset($data['discard']) || $data['discard'] == 0) 
            ) {
            unset($data['links']);
            unset($data['only']);
        }

        $game->setData($data);
        $game->save();
        $money = $game->money();
        
        return $this->renderPartial('_page', [
            'page' => $game->page,
            'game' => $game,
            'links' => $links,
            'money' => $money
        ]);
    }
    
    
    /**
     * @return string
     */
    public function actionMove($pageId, $linkId = 0)
    {
        $game = Game::getGameIdentity();
        
        $page = Page::find()
            ->where(['id' => $pageId])
            ->one();
        
        $action = Action::find()
            ->where(['id' => $linkId])
            ->one();

        foreach ($game->counters as $counter) {
            $counter->value++;
            $counter->save();
        }
        
        $game->page_id = $page->id;
        
        if ($game->page->triggersOnes) {
            foreach ($game->page->triggersOnes as $trigger) {
                if (($trigger->condition && $trigger->check()) || !$trigger->condition) {
                    $trigger->exec();
                }
            }
        }
        
        if (($action) && $action->callback && $action->callback != 'time(0)') {
            $action->exec();
        }
        
        $visited = Object::find()
            ->where(['title' => "Посещенная страница"])
            ->one();

        $game->link('objects', $visited, ['value' => $game->page->id]);
        $game->save();

        return $this->show();
    }
}
