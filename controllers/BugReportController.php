<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\Controller;
use app\models\AR\User;
use app\models\AR\BugReport;

class BugReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_TESTER,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionAjaxCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new BugReport();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'success' => true,
            ];
        }

        return [
            'success' => false,
        ];
    }
}
