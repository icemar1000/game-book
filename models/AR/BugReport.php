<?php

namespace app\models\AR;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%bug_report}}".
 *
 * @property integer $id
 * @property integer $at
 * @property integer $page_id
 * @property integer $user_id
 * @property string $message
 * @property integer $status
 *
 * @property string $userName
 * @property string $pageName
 *
 * @property Page $page
 * @property User $user
 */
class BugReport extends \yii\db\ActiveRecord
{
    const STATUS_ACTUAL = 0;
    const STATUS_FIXED = 1;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isNewRecord) {
            $this->status = self::STATUS_ACTUAL;
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bug_report}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'at',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'user_id', 'message', 'status'], 'required'],
            [['at', 'page_id', 'user_id', 'status'], 'integer'],
            [['message'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'at'      => 'Дата',
            'page_id' => 'Страница',
            'user_id' => 'Пользователь',
            'message' => 'Сообщение',
            'status'  => 'Статус',
        ];
    }

    /**
     * @param int|null $i
     * @return mixed
     */
    public static function getStatuses($i = null)
    {
        $array = [
            self::STATUS_ACTUAL => 'Актуально',
            self::STATUS_FIXED  => 'Исправлено',
        ];
        return $i === null ? $array :
                    (isset($array[$i]) ? $array[$i] : false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user ? $this->user->username : '';
    }

    /**
     * @return string
     */
    public function getPageName()
    {
        return "{$this->page->id}: {$this->page->title}";
    }
    
    /**
     * @return integer
     */
    public static function getActiveBugs()
    {
        return self::find()
            ->where(['status' => self::STATUS_ACTUAL])
            ->count();
    }
}
