<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use voskobovich\linker\LinkerBehavior;
use app\components\behaviors\IdForSaveBehavior;
use app\components\behaviors\AdminLogBehavior;
use app\models\queries\ActionQuery;

/**
 * This is the model class for table "{{%action}}".
 *
 * @property integer $id
 * @property string $category
 * @property string $text
 * @property string $condition
 * @property string $callback
 * @property integer $index
 * @property integer $from_id
 * @property integer $to_id
 *
 * @property Page $from
 * @property Page $to
 *
 * @property array $tag_ids
 *
 * @property string $tagsNames
 *
 * @property int|null $idForSave
 */
class Action extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isNewRecord) {
            $this->callback = 'time(0)';
            if ($this->category == 'common') {
                $this->callback = "time(0)\nsavePageNumber()";
            }
            $this->index = 1;
        }
        parent::init();
    }

    /**
     * @return array
     */
    public static function getCategories()
    {
        return [
            'default' => 'Переход',
            'common'  => 'Общее действие',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'idForSave'  => IdForSaveBehavior::className(),
            'manyToMany' => [
                'class'     => LinkerBehavior::className(),
                'relations' => [
                    'tag_ids' => ['tags'],
                ],
            ],
            'adminLog'   => AdminLogBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%action}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_ids'], 'each', 'rule' => ['integer']],
            [['tag_ids'], function ($attribute, $params, $validator) {
                /* @var $models \app\models\AR\Tag[] */
                $models = Tag::find()->where(['id' => $this->tag_ids])->all();
                $_props = [];

                foreach ($models as $model) {
                    if (in_array($model->property, $_props)) {
                        $this->addError($attribute, 'Свойства выбраных тегов не должны повторяться.');
                    }
                    $_props[] = $model->property;
                }
            }],
            [['text', 'to_id'], 'required'],
            [['text', 'condition', 'callback'], 'string'],
            [['index', 'from_id', 'to_id', 'idForSave'], 'integer'],
            [['category'], 'string', 'max' => 255],
            [
                ['from_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Page::className(),
                'targetAttribute' => ['from_id' => 'id'],
            ],
            [
                ['to_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Page::className(),
                'targetAttribute' => ['to_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'category'  => 'Категория',
            'text'      => 'Текст',
            'condition' => 'Условие',
            'callback'  => 'Скрипт',
            'index'     => 'Приоритет',
            'from_id'   => 'Со страницы',
            'to_id'     => 'На страницу',
            'tag_ids'   => 'Теги',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(Page::className(), ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(Page::className(), ['id' => 'to_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this
            ->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('{{%action_tag}}', ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsIds()
    {
        return ArrayHelper::map($this->tags, 'id', 'id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsNames()
    {
        return implode(', ', ArrayHelper::map($this->tags, 'id', 'title'));
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ActionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActionQuery(get_called_class());
    }
    
    public function exec()
    {
        $buy = Trigger::buy();
        $changeMoney = Trigger::changeMoney();
        $drawCard = Trigger::drawCard();
        $drawJoker = Trigger::drawJoker();
        $discardCard = Trigger::discardCard();
        $sellCard = Trigger::sellCard();
        $addCounter = Trigger::addCounter();
        $giveObject = Trigger::giveObject();
        $sellObject = Trigger::sellObject();
        $newGame = Trigger::newGame();
        $dropObject = Trigger::dropObject();
        $dropAll = Trigger::dropAll();
        $discardItem = Trigger::discardItem();
        $clearData = Trigger::clearData();

        eval($this->callback);
    }
    
    public function check()
    {
        $dontShow = Trigger::dontShow();
        $checkCard = Trigger::checkCard();
        $checkObject = Trigger::checkObject();
        $checkValue = Trigger::checkValue();
        $getValue = Trigger::getValue();
        $checkItems = Trigger::checkItems();
        $checkVisitedPages = Trigger::checkVisitedPages();
        
        if ($this->condition){
            return eval('return '.$this->condition);
        }
        return true;
    }
}
