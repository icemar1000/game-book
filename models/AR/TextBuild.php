<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use app\components\behaviors\FlashMessagesBehavior;
use app\components\behaviors\AdminLogBehavior;
use app\models\AR\AdminLog;
use app\models\AR\Tag;
use app\models\queries\TextBuildQuery;

/**
 * This is the model class for table "{{%text_build}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property array $settings
 * @property integer $status
 *
 * @property AdminLog $log
 * @property string $createdAt
 * @property string $userName
 * @property string $statusName
 */
class TextBuild extends ActiveRecord
{
    const OPTION_TAGS_ALLOWED      = 'tags_allowed';
    const OPTION_TAGS_LINKS_ON_TOP = 'tags_links_on_top';
    const OPTION_ASSET_BUNDLE      = 'asset_bundle';

    const STATUS_IN_WORK    = 1;
    const STATUS_COMPLETE   = 2;
    const STATUS_FAILED     = 0;
    const STATUS_NO_CONTENT = -1;

    public $settingsAsArray = null;

    /**
     * @return string|null
     */
    public function getCssFile()
    {
        $asset = $this->getOptionValue(self::OPTION_ASSET_BUNDLE);

        if (!$asset) {
            return null;
        }

        return $asset::getCssFile();
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isNewRecord) {
            $this->text = 'NO DATA';
        }

        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate() {
        if ($this->settingsAsArray !== null) {
            $this->settings = Json::encode($this->settingsAsArray);
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public static function getAvailableAssetBundles()
    {
        return [
            'app\assets\game\RaceAsset' => 'app\assets\game\RaceAsset',
        ];
    }

    /**
     * @return array
     */
    public function getAvailableSettings()
    {
        return [
            self::OPTION_ASSET_BUNDLE      => 'AssetBundle',
            self::OPTION_TAGS_ALLOWED      => 'Теги, входящие в релиз',
            self::OPTION_TAGS_LINKS_ON_TOP => 'Теги, перемещающие ссылки на верх страницы',
        ];
    }

    /**
     * @return array
     */
    public function getSettingsTagsType()
    {
        return [
            self::OPTION_TAGS_ALLOWED      => Tag::TYPE_PAGE,
            self::OPTION_TAGS_LINKS_ON_TOP => Tag::TYPE_ACTION,
        ];
    }

    /**
     * @return array
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_IN_WORK    => 'Сборка',
            self::STATUS_COMPLETE   => 'Завершено',
            self::STATUS_FAILED     => 'Ошибка',
            self::STATUS_NO_CONTENT => 'Нет контента',
        ];

        return isset($array[$this->status])
            ? $array[$this->status]
            : '';
    }

    /**
     * @param string $name
     * @return array|string|null
     */
    public function getOptionValue($name)
    {
        $options = $this->getSettingsAsArray();

        return isset($options[$name])
            ? $options[$name]
            : null;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'flashMessages' => FlashMessagesBehavior::className(),
            'adminLog'      => AdminLogBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%text_build}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'title'], 'required'],
            [['text', 'settings'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['settingsAsArray'], function ($attribute, $params) {
                if (!is_array($this->$attribute)) {
                     $this->addError($attribute, 'Array excepted.');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'title'    => 'Название',
            'text'     => 'Текст',
            'settings' => 'Настройки',
            'status'   => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLog()
    {
        return $this
            ->hasOne(AdminLog::className(), ['model_id' => 'id'])
            ->where('admin_log.model = :model and admin_log.action = :action', [
                ':model'  => $this->className(),
                ':action' => AdminLog::ACTION_CREATE,
            ]);
    }

    /**
     * @return array
     */
    public function getSettingsAsArray()
    {
        if ($this->settingsAsArray === null) {
            if (!$this->settings) {
                return [];
            }

            return $this->settingsAsArray = Json::decode($this->settings, true);
        }

        return $this->settingsAsArray;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->log->at;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->log->userName;
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\TextBuildQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TextBuildQuery(get_called_class());
    }
}
