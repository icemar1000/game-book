<?php

namespace app\models\AR;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\AR\Tag;
use app\modules\admin\models\search\NodeSearch;
use voskobovich\linker\LinkerBehavior;
use app\components\behaviors\FlashMessagesBehavior;
use app\components\behaviors\AdminLogBehavior;

/**
 * This is the model class for table "{{%node}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property integer $x
 * @property integer $y
 *
 * @property string $tagNames
 *
 * @property Node[] $parents
 * @property Node[] $children
 * @property Tag[] $tags
 */
class Node extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%node}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'manyToMany'    => [
                'class'     => LinkerBehavior::className(),
                'relations' => [
                    'tag_ids' => ['tags'],
                    'parent_ids' => ['parents'],
                    'children_ids' => ['children'],
                ],
            ],
            'flashMessages' => FlashMessagesBehavior::className(),
            'adminLog'      => AdminLogBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['x', 'y'], 'integer'],
            [['tag_ids', 'parent_ids', 'children_ids'], 'each', 'rule' => ['integer']],
            [['tag_ids'], function ($attribute, $params, $validator) {
                /* @var $models app\models\AR\Tag[] */
                $models = Tag::find()->where(['id' => $this->tag_ids])->all();
                $_props = [];

                foreach ($models as $model) {
                    if (in_array($model->property, $_props)) {
                        $this->addError($attribute, 'Свойства выбраных тегов не должны повторяться.');
                    }
                    $_props[] = $model->property;
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'title'        => 'Название',
            'x'            => 'X',
            'y'            => 'Y',
            'tag_ids'      => 'Теги',
            'parent_ids'   => 'Предки',
            'children_ids' => 'Потомки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsNames()
    {
        return implode(', ', ArrayHelper::map($this->tags, 'id', 'title'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this
            ->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('{{%node_tag}}', ['node_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this
            ->hasMany(self::className(), ['id' => 'parent_id'])
            ->from(['parents' => self::tableName()])
            ->viaTable('{{%node_node}}', ['child_id' => 'id'], function ($q) {
                $q->from(['parents_link' => '{{%node_node}}']);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this
            ->hasMany(self::className(), ['id' => 'child_id'])
            ->from(['children' => self::tableName()])
            ->viaTable('{{%node_node}}', ['parent_id' => 'id'], function ($q) {
                $q->from(['children_link' => '{{%node_node}}']);
            });
    }

    /**
     * @return array
     */
    public static function getModelsList($except = null)
    {
        $query = self::find();

        if ($except !== null) {
            $query->where(['not', ['id' => $except]]);
        }

        $models = $query->all();

        return ArrayHelper::map($models, 'id', function ($model, $defaultValue) {
            /* @var $model app\models\AR\Node */
            return "{$model->title} ({$model->tagsNames})";
        });
    }
}
