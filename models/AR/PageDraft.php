<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "page_draft".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $text
 */
class PageDraft extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_draft';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],
            [['text'], 'required'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'text' => 'Text',
        ];
    }
}
