<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\components\behaviors\IdForSaveBehavior;
use app\components\behaviors\AdminLogBehavior;
use app\models\queries\TriggerQuery;

/**
 * This is the model class for table "{{%trigger}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $condition
 * @property string $callback
 * @property integer $priority
 * @property integer $page_id
 *
 * @property integer $idForSave
 *
 * @property Page $page
 * 
 * @property int|null $idForSave
 */
class Trigger extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isNewRecord) {
            $this->priority = 1;
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'idForSave' => IdForSaveBehavior::className(),
            'adminLog'  => AdminLogBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%trigger}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['callback'], 'required'],
            [['text', 'condition', 'callback'], 'string'],
            [['priority', 'page_id', 'idForSave', 'ones'], 'integer'],
            [
                ['page_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Page::className(),
                'targetAttribute' => ['page_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'title'     => 'Название',
            'text'      => 'Текст',
            'condition' => 'Условие',
            'callback'  => 'Скрипт',
            'priority'  => 'Приоритет',
            'page_id'   => 'Страница',
            'ones'      => 'Количество запусков при переходе на страницу',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\TriggerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TriggerQuery(get_called_class());
    }

    public static function buy()
    {
        return function($keyword, $price, $trigger = null) { 
            $item = Object::byTitle($keyword);
            $game = Game::getGameIdentity();
            $data = $game->getData();
            $data['links'][] = [
                'url' => Url::to([
                    'game/buy',
                    'item' => $item->id,
                    'price'=> $price,
                    'trigger' => $trigger
                ]),
                'text' => 'Купить предмет "' . $item->text . '" за ' . $price . ' кр.',
                'active' => $game->checkMoney($price) && !$game->isHaveObject($item),
            ];
            $game->setData($data);
            $game->save();
        };
    }

    public static function changeMoney()
    {
        return function ($change) {
            $money = Game::getGameIdentity()->money();
            $money->value += $change;
            $money->save();
            \Yii::$app->getSession()->setFlash('success', 'Получены кредиты: '.$change);
        };
    }

    public static function drawCard()
    {
        return function ($suit = null) {
            if (!$suit) {
                $suit = array_rand(Object::getCardSuits());
            }
            $card = Game::getGameIdentity()->drawCard($suit);
            \Yii::$app->getSession()->setFlash('success', 'Получена карта '.$card->name);
        };
    }

    public static function drawJoker()
    {
        return function () {
            $item = Object::byTitle('joker'); 

            $game = Game::getGameIdentity();
            \Yii::$app->getSession()->setFlash('success', 'Получена карта "Джокер"');
            $game->link('objects', $item, [
                'value' => 5,
                'place' => 'hand',
            ]);
        };
    }

    public static function addCounter()
    {
        return function ($keyword, $value) {
            $item = Object::byTitle($keyword); 
            $game = Game::getGameIdentity();
            if (!$game->isHaveObject($item)){
                $game->link('objects',$item,['value'=>$value]);
                
                if ($item->category==Object::$CATEGORY_DEVICE)
                    \Yii::$app->getSession()->setFlash('success', 'Получен предмет: '.$item->text);
                
                if ($item->category==Object::$CATEGORY_KEYWORD)
                    \Yii::$app->getSession()->setFlash('success', 'Получено ключевое слово: '.$item->text);
                
                if ($item->category==Object::$CATEGORY_UPGRADE)
                    \Yii::$app->getSession()->setFlash('success', 'Получен апгрейд: '.$item->text);
            }else{
                $gameobject = GameObject::find()->where(['game_id'=>$game->id])->andWhere(['object_id'=>$item->id])->one();
                $gameobject->value+=$value;
                $gameobject->save();
            }
            
        };
    }

    public static function sellCard()
    {
        return function ($price) {
            $game = Game::getGameIdentity();
            $data = $game->getData();
            foreach ($game->hand as $card) {
                $data['links'][] = [
                    'url' => Url::to([
                        'game/sell',
                        'item' => $card->id,
                        'price' => $price,
                    ]),
                    'text' => 'Продать карту "' . $card->name . '" за ' . $price . ' кр.',
                    'active' => true,
                ];
            }
            $game->setData($data);
            $game->save();
        };
    }

    public static function sellObject()
    {
        return function ($price) {
            $game = Game::getGameIdentity();
            $data = $game->getData();
            foreach ($game->items as $item) {
                $data['links'][] = [
                    'url' => Url::to([
                        'game/sell',
                        'item' => $item->id,
                        'price' => $price,
                    ]),
                    'text' => 'Продать предмет "' . $item->text . '" за ' . $price . ' кр.',
                    'active' => true,
                ];
            }
            $game->setData($data);
            $game->save();

            $game = Game::getGameIdentity();
            $data = $game->getData();
        };
    }

    public static function checkItems()
    {
        return function(){
            $game = Game::getGameIdentity();
            if ($game->items){
                return true;
            }else{
                return false;
            }
        };
    }
    
    public static function discardItem()
    {
        return function (){
            $game = Game::getGameIdentity();
            $data = $game->getData();
           
            foreach ($game->items as $item) {
                 $data['links'][] = [
                    'url' => Url::to([
                        'game/drop',
                        'title' => $item->title,
                    ]),
                    'text' => 'Выбросить ' . $item->text,
                    'active' => true,
                ];
            }
            
            $data['only'] = true;
            
            $game->setData($data);
            $game->save();
            
            return true;
        };
    }
    
    public static function discardCard()
    {
        return function ($suit = null, $minValue = null, $quantity = 1, $drawAfter = false) {
            $game = Game::getGameIdentity();
            $isHaveCard = self::checkCard();
            $data = $game->getData();
            
            $data['discard'] = $quantity;
            
            if (!$isHaveCard($suit, $minValue) && !$drawAfter) {
                unset($data['discard']);
                $data['only'] = true;
                $data['links'][] = [
                    'url' => Url::to([
                        'game/move',
                        'pageId' => 52,
                    ]),
                    'text' => 'Поражение',
                    'active' => true,
                ];
                $game->setData($data);
                $game->save();
                return false;
            }

            $hand_query = Game::getGameIdentity()
                ->getHand();
            
            if ($suit) {
                $hand_query->bySuit($suit);
            }
           
            if ($minValue) {
                $hand_query
                    ->byMinValue(GameObject::getCardValueID($minValue))
                    ->orJoker($game->id, GameObject::$CARD_PLACE_HAND);
            }
           
            $cards = $hand_query->all();
            
            foreach ($cards as $card) {
                $data['links'][] = [
                    'url' => Url::to([
                        'game/discard',
                        'value' => $card->value,
                        'suit' => $card->object->title,
                        'drawAfter' => $drawAfter,
                    ]),
                    'text' => 'Сбросить ' . $card->name . ($drawAfter ? ' и получить две карты другой масти' : ''),
                    'active' => true,
                ];
            }

            if (!$drawAfter) {
                $data['only'] = true;
            }
            
            $game->setData($data);
            $game->save();

            return true;
        };
    }

    public static function giveObject()
    {
        return function ($title = '') {
            $item = Object::byTitle($title); 
            $game = Game::getGameIdentity();
            $game->link('objects',$item);
            if ($item->category==Object::$CATEGORY_DEVICE)
                \Yii::$app->getSession()->setFlash('success', 'Получен предмет: '.$item->text);

            if ($item->category==Object::$CATEGORY_KEYWORD)
                \Yii::$app->getSession()->setFlash('success', 'Получено ключевое слово: '.$item->text);

            if ($item->category==Object::$CATEGORY_UPGRADE)
                \Yii::$app->getSession()->setFlash('success', 'Получен апгрейд: '.$item->text);
            return true;
        };
    }

    public static function dropAll()
    {
        return function () {
            $game = Game::getGameIdentity();
            $game->unlinkAll('objects', true);
            $game->initGame();
            $game->save();
            return true;
        };
    }    

    public static function dontShow()
    {
        return function ($trigger = true) {
           $checkCard = self::checkCard();
           $checkObject = self::checkObject();
           $checkValue = self::checkValue();
           $getValue = self::getValue();
           return eval('return ' . $trigger) ? true : null;
        };
    }

    public static function checkCard()
    {
        return function ($suit = null, $minValue = null) {
            $game = Game::getGameIdentity();
            $hand_query = Game::getGameIdentity()
                ->getHand()
                ->orderBy(['value' => SORT_ASC]);

            if ($suit) {
                $hand_query->bySuit($suit);
            }

            if ($minValue) {
                $hand_query
                    ->byMinValue(GameObject::getCardValueID($minValue))
                    ->orJoker($game->id, GameObject::$CARD_PLACE_HAND);
            }
           
            $hand = $hand_query->one();
            return $hand ? true : false;
        };
    }

    public static function checkObject()
    {
        return function ($item, $destroy = false) {
            $item = Object::byTitle($item);
            if ($destroy) {
                Yii::$app->user->identity->game->unlink('objects', $item, true);
            }
            return Yii::$app->user->identity->game->isHaveObject($item);
        };
    }

    public static function checkValue()
    {
        return function ($item, $minValue) {
            $item = Object::byTitle($item);
            $game = Game::getGameIdentity();
            $gameobject = GameObject::find()
                ->where(['game_id' => $game->id])
                ->andWhere(['object_id' => $item->id])
                ->one();

            if (!$gameobject) {
                return false;
            }

            return $gameobject->value >= $minValue;
        };
    }

    public static function newGame()
    {
        return function () {
            Yii::$app->user->identity->game->delete();
            $game = new Game();
            $game->page_id = 3;
            $game->save();
        };
    }

    public static function dropObject()
    {
        return function ($title) {
            $item = Object::byTitle($title); 
            $game = Game::getGameIdentity();
            if ($game->isHaveObject($item)) {
                $game->unlink('objects', $item, true);
            }
        };
    }

    public static function getValue()
    {
        return function ($item) {
            $item = Object::byTitle($item);
            $game = Game::getGameIdentity();
            $gameobject = GameObject::find()
                ->where(['game_id' => $game->id])
                ->andWhere(['object_id' => $item->id])
                ->one();
            if (!$gameobject) {
                return 0;
            }
            return $gameobject->value;
        };
    }

    public static function maxSuit()
    {
        return function ($title) {
            $item = Object::byTitle($title);
            $game = Game::getGameIdentity();
            $gameobject = GameObject::find()
                ->where(['game_id' => $game->id])
                ->andWhere(['object_id' => $item->id])
                ->one();
            if (!$gameobject) {
                return 0;
            }
            return $gameobject->value;
        };
    }    

    public static function randMove()
    {
        return function ($showActions) {
            $game = Game::getGameIdentity();
            $data = $game->getData();
            $data=[];
            $data['only'] = true;
            $allActions = Action::find()
                ->where(['from_id' => $game->page])
                ->all();

            foreach ($showActions as $actionId) {
                $action = Action::find()
                    ->where(['id'=>$actionId])
                    ->one();
                $data['links'][] = [
                    'url' => Url::to([
                        'game/move',
                        'pageId' => $action->to_id,
                        'linkId' => $action->id
                    ]),
                    'text' => $action->text,
                    'active' => true,
                ];
            }
            
            $_allActions = $allActions;
            foreach ($_allActions as $key => $action) {
                if (in_array($action->id, $showActions) || in_array($action->to_id, $game->visited) || !$action->check()) {
                    unset($allActions[$key]);
                }
            }

            if (!empty($allActions)) {
                $action = $allActions[array_rand($allActions)];
                $data['links'][] = [
                    'url' => Url::to([
                        'game/move',
                        'pageId' => $action->to_id,
                        'linkId' => $action->id
                    ]),
                    'text' => $action->text,
                    'active' => ($action->condition) ? $action->check() : true,
                ];
            }
            $game->setData($data);
            $game->save();
            return true;
        };
    }    
    
    public static function drawCardFromDiscard(){
        return function () {
            $game = Game::getGameIdentity();
            $data = $game->getData();
            if ($game->discard){
                $data['only'] = true;
                $data['drawFromDiscard'] = true;
                foreach($game->discard as $card){
                    $data['links'][]=[
                        'url'=>  \yii\helpers\Url::to([
                            'game/draw',
                            'value' => $card->value,
                            'suit'=> $card->object->title,
                            'quantity' => 1
                        ]),
                        'text'=>'Взять '.$card->name.' из сброса',
                        'active'=>true
                    ];
                }
                $game->setData($data);
                $game->save();
                return true;
            }
            return false;
        };
    }
    
    public static function checkVisitedPages(){
        return function ($pageIds) {
            $game = Game::getGameIdentity();
            $item = Object::byTitle("Посещенная страница");
            foreach ($pageIds as $pageId){
                $model = GameObject::find()->where([
                    'game_id'=>$game->id,
                    'object_id'=>$item->id,
                    'value' => $pageId
                ])->one();
                if (!$model) return false;
            }
            return true;
        };
    }
    
    public static function clearQuantumPages(){
        return function () {
            $game = Game::getGameIdentity();
            $item = Object::byTitle("Посещенная страница");
            $pageIds = [548,553,558,566,563];
            foreach ($pageIds as $pageId){
                $model = GameObject::find()->where([
                    'game_id'=>$game->id,
                    'object_id'=>$item->id,
                    'value' => $pageId
                ])->one();
                if ($model) $model->delete();
            }
        };
    }
    
    public static function getRandomKeyword(){
        return function () {
            $game = Game::getGameIdentity();
            $data = $game->getData();
            $keywords = Object::find()->where(['category'=>  Object::$CATEGORY_KEYWORD])->all();
            $func = self::checkObject();
            foreach ($keywords as $key => $keyword){
                if ($func($keyword->title)){
                    unset($keywords[$key]);
                }
            }
            $choosen = array_rand($keywords, 3);
            $data['only'] = true;
            $data['getKeyword'] = true;
            foreach ($choosen as $key){
                $keyword = $keywords[$key];
                $data['links'][]=[
                    'url'=>  \yii\helpers\Url::to([
                        'game/getkeyword',
                        'title' => $keyword->title,
                    ]),
                    'text'=>$keyword->text,
                    'active'=>true
                ];
            }
            $game->setData($data);
            $game->save();
        };
    }
    
    public static function clearData(){
        return function () {
            $game = Game::getGameIdentity();
            $game->setData([]);
            $game->save();
        };
    }
    
    public static function addAchievement(){
        return function($name){
            $user = Game::getGameIdentity()->user;
            $achievement = Achievement::find()->where(['name'=>$name])->one();
            
            if ($achievement and !in_array($achievement, $user->achievements)){
                \Yii::$app->getSession()->setFlash('success', 'Получено достижение: "'. $achievement->name . 
                        '"<br>' . $achievement->text);
                $user->link("achievements", $achievement, ['date'=> time()]);
            }
        };
    }
    
    public function exec()
    {
        //+
        $buy = self::buy();
        //+
        $changeMoney = self::changeMoney();
        //+
        $drawCard = self::drawCard();
        //+
        $discardCard = self::discardCard();
        //+
        $addCounter = self::addCounter();
        //+
        $giveObject = self::giveObject();
        //+
        $discardItem = self::discardItem();
        //+
        $drawJoker = self::drawJoker();
        //+
        $clearQuantumPages = self::clearQuantumPages();
        //+
        $dropObject = self::dropObject();
        //+
        $dropAll = self::dropAll();
        //+
        $drawCardFromDiscard = self::drawCardFromDiscard();
        //+
        $getRandomKeyword = self::getRandomKeyword();
        //+
        $sellCard = self::sellCard();
        //+
        $sellObject = self::sellObject();
        //+
        $randMove = self::randMove();
        
        $clearData = self::clearData();
        $newGame = self::newGame();
        $addAchievement = self::addAchievement();
        
        eval($this->callback);
    }

    public function check()
    {
        //+
        $getValue = self::getValue();
        //+
        $checkValue = self::checkValue();
        //+
        $checkObject = self::checkObject();         
        //+
        $checkCard = self::checkCard();
        //+
        $checkItems = self::checkItems();
        //+
        $checkVisitedPages = self::checkVisitedPages();
        //+
        $dontShow = self::dontShow();
        
        return eval('return ' . $this->condition);
    }
}
