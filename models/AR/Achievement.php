<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "achievement".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 *
 * @property AchievementUser[] $achievementUsers
 */
class Achievement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'achievement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['name', 'text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAchievementUsers()
    {
        return $this->hasMany(AchievementUser::className(), ['achievement_id' => 'id']);
    }
}
