<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\components\behaviors\FlashMessagesBehavior;
use app\components\behaviors\AdminLogBehavior;
use app\models\queries\TagQuery;

/**
 * This is the model class for table "{{%tag}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $category
 * @property string $property
 * @property string $value
 *
 * @property string $categoryName
 * @property string $propertyName
 *
 * @property Action[] $actions
 * @property Page[] $pages
 * @property Node[] $nodes
 */
class Tag extends ActiveRecord
{
    const TYPE_NODE   = 'node';
    const TYPE_PAGE   = 'page';
    const TYPE_ACTION = 'action';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'flashMessages' => FlashMessagesBehavior::className(),
            'adminLog'      => AdminLogBehavior::className(),
        ];
    }

    /**
     * @param string|null $i
     * @return array|string|false
     */
    public static function getCategories($i = null)
    {
        $array = [
            'node'   => 'Для узлов',
            'page'   => 'Для страниц',
            'action' => 'Для переходов',
        ];

        return $i === null
            ? $array
            : (isset($array[$i]) ? $array[$i] : false);
    }

    /**
     * @param string $i
     * @return array
     */
    public static function getProperties($i = null)
    {
        $node = [
            'color.border'                 => 'Цвет рамки',
            'color.background'             => 'Цвет фона',
            'font.color'                   => 'Цвет шрифта',
            'shape'                        => 'Фигура',
            'shapeProperties.borderDashes' => 'Прерывистые края',
        ];

        $array = [
            'node'   => $node,
            'page'   => $node,
            'action' => [
                'color.color' => 'Цвет линии',
                'dashes'      => 'Прерывистая линия',
            ],
        ];

        return $i === null
            ? array_merge($array['page'], $array['action'])
            : (isset($array[$i]) ? $array[$i] : []);
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        $categories = self::getCategories();
        return isset($categories[$this->category]) ? $categories[$this->category] : '';
    }

    /**
     * @return string
     */
    public function getPropertyName()
    {
        $properties = $this->getAvailableProperties();
        return isset($properties[$this->property]) ? $properties[$this->property] : '';
    }

    /**
     * @param bool $all
     * @return array
     */
    public function getAvailableProperties()
    {
        return self::getProperties($this->category);
    }

    /**
     * @return string|array
     */
    public function getAvailablePropertyValues()
    {
        switch ($this->property) {
            case 'color.border':
            case 'color.background':
            case 'font.color':
            case 'color.color':
                return 'color';
            case 'shapeProperties.borderDashes':
            case 'dashes':
                return [
                    1 => 'Да',
//                  0 => 'Нет',
                ];
            case 'shape':
                return [
                    'dot'          => 'Круг',
                    'diamond'      => 'Ромб',
                    'square'       => 'Квадрат',
                    'star'         => 'Звезда',
                    'triangle'     => 'Треугольник',
                    'triangleDown' => 'Перевернутый треугольник',
                ];
        }
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $availableValues = $this->getAvailablePropertyValues();
        return [
            [['category'], 'in', 'range' => array_keys(self::getCategories())],
            [['property'], 'in', 'range' => array_keys($this->getAvailableProperties())],
            [
                ['value'],
                'in',
                //Костыль
                'range' => is_array($availableValues) ? array_keys($availableValues) : [],
                'when'  => function ($model, $attribute) {
                    /* @var $model app\models\AR\Tag */
                    return $model->getAvailablePropertyValues() !== 'color';
                },
            ],
            [['title', 'category', 'property', 'value'], 'required'],
            [['title', 'category', 'property', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'title'    => 'Название',
            'category' => 'Категория',
            'property' => 'Свойство',
            'value'    => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActions()
    {
        return $this
            ->hasMany(Action::className(), ['id' => 'action_id'])
            ->viaTable('{{%action_tag}}', ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this
            ->hasMany(Page::className(), ['id' => 'page_id'])
            ->viaTable('{{%page_tag}}', ['tag_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNodes()
    {
        return $this
            ->hasMany(Node::className(), ['id' => 'node_id'])
            ->viaTable('{{%node_tag}}', ['tag_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\TagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TagQuery(get_called_class());
    }

    /**
     * @param string $category
     * @return array
     */
    public static function getModelsListByCategory($category)
    {
        $models = Tag::find()->where(['category' => $category])->all();
        return ArrayHelper::map($models, 'id', function ($model, $defaultValue) {
            return "{$model->title} ({$model->propertyName})";
        });
    }
}
