<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use app\models\queries\ObjectQuery;

/**
 * This is the model class for table "{{%object}}".
 *
 * @property integer $id
 * @property string $category
 * @property string $title
 * @property string $text
 * @property integer $value
 * @property integer $parent_id
 *
 * @property Object $parent
 * @property Object[] $objects
 */
class Object extends ActiveRecord
{
    public static $CATEGORY_CARD = 'Карта';
    public static $CATEGORY_DEVICE = 'Предмет';
    public static $CATEGORY_UPGRADE = 'Улучшение';
    public static $CATEGORY_KEYWORD = 'Ключевое слово';
    public static $CATEGORY_MARKER = 'Маркер';
    public static $CATEGORY_MONEY = 'Деньги';
    public static $CATEGORY_COUNTER = 'Счетчик';

    public static $CARD_SUIT_HEART = 'hearts';
    public static $CARD_SUIT_SPADE = 'spades';
    public static $CARD_SUIT_CLUB = 'clubs';
    public static $CARD_SUIT_DIAMOND = 'diamonds';
    public static $CARD_JOKER = 'joker';

    public static $MONEY = 'money';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%object}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text'], 'string'],
            [['value', 'parent_id'], 'integer'],
            [['category', 'title'], 'string', 'max' => 255],
            [
                ['parent_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Object::className(),
                'targetAttribute' => ['parent_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'category'  => 'Категория',
            'title'     => 'Название',
            'text'      => 'Текст',
            'value'     => 'Значение',
            'parent_id' => 'Предок',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Object::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Object::className(), ['parent_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ObjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ObjectQuery(get_called_class());
    }

    public static function byTitle($title)
    {
        return self::find()
            ->where(['title' => $title])
            ->one();
    }

    public static function byID($id)
    {
        return self::find()
            ->where(['id' => $id])
            ->one();
    }

    public static function cardHearts()
    {
        return self::byTitle(self::$CARD_SUIT_HEART);
    }

    public static function money()
    {
        return self::byTitle(self::$MONEY);
    }

    public static function cardClubs()
    {
        return self::byTitle(self::$CARD_SUIT_CLUB);
    }

    public static function cardSpades()
    {
        return self::byTitle(self::$CARD_SUIT_SPADE);
    }

    public static function cardDiamonds()
    {
        return self::byTitle(self::$CARD_SUIT_DIAMOND);
    }

    public static function getCardSuits()
    {
        return [
            self::$CARD_SUIT_HEART => 'hearts',
            self::$CARD_SUIT_SPADE => 'spades',
            self::$CARD_SUIT_CLUB => 'clubs',
            self::$CARD_SUIT_DIAMOND => 'diamonds',
        ];
    }

    public static function getCategories()
    {
        return [
            self::$CATEGORY_DEVICE => 'Предмет',
            self::$CATEGORY_UPGRADE => 'Улучшение',
            self::$CATEGORY_MARKER => 'Маркер',
            self::$CATEGORY_COUNTER => 'Счетчик',
            self::$CATEGORY_KEYWORD => 'Ключевое слово',
        ];
    }
}
