<?php

namespace app\models\AR;

use Yii;
use app\models\AR\GameObject;
use app\models\AR\Object;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $user_id
 * @property string $data
 *
 * @property User $user
 * @property Page $page
 * @property Objects $objects
 * 
 */
class Game extends \yii\db\ActiveRecord
{
    public static $actualTags = [9, 11, 12, 21, 13, 24, 25, 26];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->page_id = 584;
    }

    /**
     * @inheritdoc
     */
    public function initQuantum()
    {
        $this->page_id = 319;
    }

    public function initGame()
    {
        //наполнение деки картами
        for ($i = 0; $i <= 4; $i++) {
            $this->link('objects', Object::cardHearts(), ['place' => 'deck', 'value' => $i]);
        }
        for ($i = 0; $i <= 4; $i++) {
            $this->link('objects', Object::cardSpades(), ['place' => 'deck', 'value' => $i]);
        }
        for ($i = 0; $i <= 4; $i++) {
            $this->link('objects', Object::cardClubs(), ['place' => 'deck', 'value' => $i]);
        }
        for ($i = 0; $i <= 4; $i++) {
            $this->link('objects', Object::cardDiamonds(), ['place' => 'deck', 'value' => $i]);
        }
        
        //выдача 0 монет.
        $this->link('objects', Object::money(), ['value' => 0]);
        
        //навешивание счетчиков по мастям
        $this->link('objects', Object::find()->where(['title' => 'Маркер. Черви'])->one(), ['value' => 0]);
        $this->link('objects', Object::find()->where(['title' => 'Маркер. Буби'])->one(), ['value' => 0]);
        $this->link('objects', Object::find()->where(['title' => 'Маркер. Трефы'])->one(), ['value' => 0]);
        $this->link('objects', Object::find()->where(['title' => 'Маркер. Пики'])->one(), ['value' => 0]);
    }

    public function money()
    {
        return GameObject::find()
            ->money()
            ->byGame($this->id)
            ->one();
    }

    public function checkDeck($suit, $value)
    {
        return GameObject::find()
            ->bySuit($suit)
            ->byValue($value)
            ->byPlace(GameObject::$CARD_PLACE_DECK)
            ->byGame($this->id)
            ->one();
    }

    public function checkDiscard($suit, $value)
    {
        return GameObject::find()
            ->bySuit($suit)
            ->byValue($value)
            ->byPlace(GameObject::$CARD_PLACE_DISCARD)
            ->byGame($this->id)
            ->one();
    }

    public function checkHand($suit, $value)
    {
        $query = GameObject::find()
            ->bySuit($suit)
            ->byValue($value)
            ->byPlace(GameObject::$CARD_PLACE_HAND)
            ->byGame($this->id);

        if ($value !== null) {
            $query->orJoker($this->id, GameObject::$CARD_PLACE_HAND);
        }

        return $query->one();
    }

    public function checkMoney($value)
    {
        $money = GameObject::find()
            ->money()
            ->byGame($this->id)
            ->one();
        return $money->value >= $value;
    }

    public function drawCard($suit, $value = null)
    {
        if (!$value) {
            $cards = GameObject::find()
                ->bySuit($suit)
                ->byGame($this->id)
                ->all();
            $card = $cards[array_rand($cards)];
        } else {
            $card = $this->checkDeck($suit, $value);
            if (!$card){
                $card = $this->checkDiscard($suit, $value);
            }
        }
        
        if ($card) {
            $card->place = GameObject::$CARD_PLACE_HAND;
            $card->save();
            return $card;
        }
       
        return false;
    }

    public function discardCard($suit, $value)
    {
        $card = $this->checkHand($suit, $value);
        if ($card) {
            $card->place = GameObject::$CARD_PLACE_DISCARD;
            $card->save();
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['page_id', 'user_id'],
                'integer'
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'user_id' => 'User ID',
        ];
    }

    public function getData()
    {
        return json_decode($this->data, true);
    }

    public function setData($data)
    {
        $this->data = json_encode($data);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this
            ->hasMany(Object::className(), ['id' => 'object_id'])
            ->viaTable('game_object', ['game_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this
            ->hasMany(Object::className(), ['id' => 'object_id'])
            ->andWhere(['category' => Object::$CATEGORY_DEVICE])
            ->viaTable('game_object', ['game_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this
            ->hasMany(Object::className(), ['id' => 'object_id'])
            ->andWhere(['category' => Object::$CATEGORY_KEYWORD])
            ->viaTable('game_object', ['game_id' => 'id']);
    } 
    /**
     * @return \yii\db\ActiveQuery
     */    
    public function getUpgrades()
    {
        return $this
            ->hasMany(Object::className(), ['id' => 'object_id'])
            ->andWhere(['category' => Object::$CATEGORY_UPGRADE])
            ->viaTable('game_object', ['game_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */    
    public function getDeck()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->andWhere(['place' => GameObject::$CARD_PLACE_DECK]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */    
    public function getDiscard()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->andWhere(['place' => GameObject::$CARD_PLACE_DISCARD]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHand()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->andWhere(['place' => GameObject::$CARD_PLACE_HAND]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVisited()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->joinWith('object')
            ->andWhere(['object.title' => "Посещенная страница"])
            ->asArray()
            ->select('game_object.value')
            ->column();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounters()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->joinWith('object')
            ->andWhere(['category' => Object::$CATEGORY_COUNTER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarkers()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->joinWith('object')
            ->andWhere(['category' => Object::$CATEGORY_MARKER]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShowMarkers()
    {
        return $this
            ->hasMany(GameObject::className(), ['game_id' => 'id'])
            ->joinWith('object')
            ->andWhere([
                'or', 
                ['object.title' => "Маркер. Буби"],
                ['object.title' => "Маркер. Пики"],
                ['object.title' => "Маркер. Черви"],
                ['object.title' => "Маркер. Трефы"],
            ])
            ->andWhere(['category' => Object::$CATEGORY_MARKER]);
    }

    /**
     * @return bool
     */
    public function isHaveObject(Object $item)
    {
        return in_array($item, $this->objects);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) { 
            $this->initGame();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        $this->unlinkAll('objects', true);

        return true;
    }

    /**
     * @return self
     */
    public static function getGameIdentity()
    {
        return Yii::$app->user->identity->game;
    }
}
