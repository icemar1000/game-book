<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "game_object".
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $game_id
 * @property integer $value
 * @property string $place
 * 
 * @property Object $object
 */
class GameObject extends \yii\db\ActiveRecord
{
    public static $CARD_VALUE_TEN = 0;
    public static $CARD_VALUE_JACK = 1;
    public static $CARD_VALUE_QUEEN = 2;
    public static $CARD_VALUE_KING = 3;
    public static $CARD_VALUE_ACE = 4;
    public static $CARD_VALUE_JOKER = 5;

    public static $CARD_PLACE_HAND = 'hand';
    public static $CARD_PLACE_DISCARD = 'discard';
    public static $CARD_PLACE_DECK = 'deck';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'game_id', 'value'], 'integer'],
            [['place'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Object ID',
            'game_id' => 'Game ID',
            'value' => 'Value',
            'place' => 'Place',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\GameObjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\GameObjectQuery(get_called_class());
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Object::className(), ['id' => 'object_id']);
    }
    
    public static function getCardValueLabels(){
        return [
            self::$CARD_VALUE_TEN   => 'десятка',
            self::$CARD_VALUE_JACK  => 'валет',
            self::$CARD_VALUE_QUEEN => 'дама',
            self::$CARD_VALUE_KING  => 'король',
            self::$CARD_VALUE_ACE   => 'туз',
            self::$CARD_VALUE_JOKER => '#',
        ];
    }
    
    public static function getCardValueID($value=null){
        $array = [
            'ten'   => self::$CARD_VALUE_TEN,
            'jack'  => self::$CARD_VALUE_JACK,
            'queen' => self::$CARD_VALUE_QUEEN,
            'king'  => self::$CARD_VALUE_KING,
            'ace'   => self::$CARD_VALUE_ACE,
            'joker' => self::$CARD_VALUE_JOKER,
        ];
        return $value ? $array[$value] : $array;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getName()
    {
        return '"' . $this->getCardValueLabels()[$this->value] . ' ' . $this->object->text . '"';
    }
}
