<?php

namespace app\models\AR;

use Yii;

/**
 * This is the model class for table "{{%admin_log}}".
 *
 * @property integer $id
 * @property integer $at
 * @property integer $user_id
 * @property string $model
 * @property string $action
 * @property integer $model_id
 *
 * @property string $actionName
 * @property string $modelName
 * @property string $userName
 *
 * @property User $user
 */
class AdminLog extends \yii\db\ActiveRecord
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['at', 'user_id', 'model', 'action', 'model_id'], 'required'],
            [['at', 'user_id', 'model_id'], 'integer'],
            [['model', 'action'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'at'       => 'Дата',
            'user_id'  => 'Пользователь',
            'model'    => 'Сущность',
            'action'   => 'Действие',
            'model_id' => 'ID сущности',
        ];
    }

    /**
     * @return array
     */
    public static function getActions()
    {
        return [
            self::ACTION_CREATE => 'Создание',
            self::ACTION_UPDATE => 'Обновление',
            self::ACTION_DELETE => 'Удаление',
        ];
    }

    /**
     * @return array
     */
    public static function getModelClasses()
    {
        return [
            'app\models\AR\Tag'       => 'Тег',
            'app\models\AR\Page'      => 'Страница',
            'app\models\AR\Node'      => 'Узел',
            'app\models\AR\Object'    => 'Объект',
            'app\models\AR\Action'    => 'Переход',
            'app\models\AR\Trigger'   => 'Триггер',
            'app\models\AR\TextBuild' => 'Сборка',
        ];
    }

    /**
     * @return string
     */
    public function getActionName()
    {
        if (in_array($this->action, array_keys($list = self::getActions()))) {
            return $list[$this->action];
        }
        return '';
    }

    /**
     * @return string
     */
    public function getModelName()
    {
        if (in_array($this->model, array_keys($list = self::getModelClasses()))) {
            return $list[$this->model];
        }
        return '';
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user ? $this->user->username : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
