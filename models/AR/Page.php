<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use voskobovich\linker\LinkerBehavior;
use app\components\behaviors\RelatedModelsBehavior;
use app\components\behaviors\FlashMessagesBehavior;
use app\components\behaviors\AdminLogBehavior;
use app\models\queries\PageQuery;
use app\modules\admin\models\search\ActionSearch;
use app\modules\admin\models\search\TriggerSearch;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property string $category
 * @property string $title
 * @property string $text
 * @property integer $x
 * @property integer $y
 * @property integer $number
 *
 * @property string $categoryName
 * @property string $tagsNames
 *
 * @property Action[] $outputLinks
 * @property Action[] $inputLinks
 * @property Trigger[] $triggers
 * @property Tag[] $tags
 *
 * @property ActiveDataProvider $inputLinksDataProvider
 *
 * @property array $tag_ids
 *
 * @method array getRelated(string $name)
 * @method bool loadWithRelated(array $data, string $formName = null)
 */
class Page extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->isNewRecord) {
            $this->category = 'default';
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'manyToMany'    => [
                'class'     => LinkerBehavior::className(),
                'relations' => [
                    'tag_ids' => ['tags'],
                ],
            ],
            'relatedModels' => [
                'class'     => RelatedModelsBehavior::className(),
                'relations' => [
                    [
                        'class'     => Action::className(),
                        'attribute' => 'from_id',
                        'relation'  => 'outputLinks',
                        'errorMsg'  => 'Ошибки при сохранении «Переходы на другие страницы».',
                        'with'      => ['tag_ids'],
                    ],
                    [
                        'class'     => Trigger::className(),
                        'attribute' => 'page_id',
                        'relation'  => 'triggers',
                        'errorMsg'  => 'Ошибки при сохранении «Триггеры».',
                    ],
                ],
            ],
            'flashMessages' => FlashMessagesBehavior::className(),
            'adminLog'      => AdminLogBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @return array
     */
    public static function getCategories()
    {
        return [
            'default'   => 'По-умолчанию',
            'beginning' => 'Начало',
            'ending'    => 'Концовка',
        ];
    }

    /**
     * @return array
     */
    public static function getPagesList()
    {
        $models = parent::find()
            ->select(['id', 'title'])
            ->all();
        return ArrayHelper::map($models, 'id', function ($model) {
            return "{$model->id}: {$model->title}";
        });
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
        $categories = self::getCategories();
        return isset($categories[$this->category]) ? $categories[$this->category] : '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_ids'], 'each', 'rule' => ['integer']],
            [['tag_ids'], function ($attribute, $params, $validator) {
                /* @var $models \app\models\AR\Tag[] */
                $models = Tag::find()->where(['id' => $this->tag_ids])->all();
                $_props = [];

                foreach ($models as $model) {
                    if (in_array($model->property, $_props)) {
                        $this->addError($attribute, 'Свойства выбраных тегов не должны повторяться.');
                    }
                    $_props[] = $model->property;
                }
            }],
            [['text'], 'required'],
            [['text'], 'string'],
            [['x', 'y', 'number'], 'integer'],
            [['category', 'title'], 'string', 'max' => 255],
            [['category'], 'in', 'range' => array_keys(self::getCategories())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'category'    => 'Категория',
            'title'       => 'Название',
            'text'        => 'Текст',
            'x'           => 'X',
            'y'           => 'Y',
            'outputLinks' => 'Переходы',
            'triggers'    => 'Триггеры',
            'tag_ids'     => 'Теги',
            'input_ids'   => 'Входящие ссылки',
            'output_ids'  => 'Исходящие ссылки',
            'number'      => 'Номер',
        ];
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function getInputLinksDataProvider()
    {
        return (new ActionSearch())->search([
            'ActionSearch' => [
                'to_id' => $this->id,
            ],
        ]);
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function getOutputLinksDataProvider()
    {
        return (new ActionSearch())->search([
            'ActionSearch' => [
                'from_id' => $this->id,
            ],
        ]);
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function getTriggersDataProvider()
    {
        return (new TriggerSearch())->search([
            'TriggerSearch' => [
                'page_id' => $this->id,
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutputLinks()
    {
        return $this->hasMany(Action::className(), ['from_id' => 'id']);
    }

    /**
     * @param \app\models\AR\Page $page
     * @param boolean $top
     * @return array
     */
    public function getLinksByPosition($tags)
    {
        $links = [
            'top'    => [],
            'bottom' => [],
        ];

        foreach ($this->outputLinks as $link) {
            $position = empty(array_intersect($tags, $link->getTagsIds()))
                ? 'bottom'
                : 'top';
            $links[$position][] = $link;
        }

        return $links;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInputLinks()
    {
        return $this->hasMany(Action::className(), ['to_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriggers()
    {
        return $this->hasMany(Trigger::className(), ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriggersOnes()
    {
        return $this->hasMany(Trigger::className(), ['page_id' => 'id'])->andWhere(['ones'=>1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTriggersMulti()
    {
        return $this->hasMany(Trigger::className(), ['page_id' => 'id'])->andWhere(['ones'=>0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this
            ->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('{{%page_tag}}', ['page_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsNames()
    {
        return implode(', ', ArrayHelper::map($this->tags, 'id', 'title'));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsIds()
    {
        return ArrayHelper::map($this->tags, 'id', 'id');
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }
}
