<?php

namespace app\models\queries;
use app\models\AR\Object;

/**
 * This is the ActiveQuery class for [[\app\models\AR\GameObject]].
 *
 * @see \app\models\AR\GameObject
 */
class GameObjectQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\models\AR\GameObject[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AR\GameObject|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function card()
    {
        return $this
            ->joinWith('object')
            ->andWhere(['category' => Object::$CATEGORY_CARD]);
    }
    
    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function money()
    {
        return $this
            ->joinWith('object')
            ->andWhere(['title' => Object::$MONEY]);
    }

    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function byGame($id)
    {
        return $this->andWhere(['game_id' => $id]);
    }

    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function bySuit($suit)
    {
        return $this
            ->card()
            ->andWhere(['title' => $suit]);
    }
    
    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function byValue($value)
    {
        return $this->andWhere(['game_object.value' => $value]);
    }
    
    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function byMinValue($value)
    {
        return $this->andWhere(['>=', 'game_object.value', $value]);
    }
    
    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function byPlace($place)
    {
        return $this->andWhere(['place' => $place]);
    }

    /**
     * @return \app\models\queries\GameObjectQuery
     */
    public function orJoker($id, $place)
    {
        return $this->orWhere([
            'and',
            ['title' => Object::$CARD_JOKER],
            ['game_id' => $id],
            ['place' => $place],
        ]);
    }
}
