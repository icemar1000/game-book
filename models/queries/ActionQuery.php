<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\AR\Action]].
 *
 * @see \app\models\AR\Action
 */
class ActionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\AR\Action[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AR\Action|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
