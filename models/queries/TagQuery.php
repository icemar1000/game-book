<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\AR\Tag]].
 *
 * @see \app\models\AR\Tag
 */
class TagQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\AR\Tag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AR\Tag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
