<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\AR\Object]].
 *
 * @see \app\models\AR\Object
 */
class ObjectQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\AR\Object[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AR\Object|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
