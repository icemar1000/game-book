<?php

namespace app\models\queries;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\AR\TextBuild]].
 *
 * @see \app\models\AR\TextBuild
 */
class TextBuildQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return \app\models\AR\TextBuild[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AR\TextBuild|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
