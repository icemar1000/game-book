var canvas;
var context;
var screenH;
var screenW;
var timer;
var stars = [];
var fps = 50;
var numStars = 4500;
var shootingStars = [],
    shootingStarSpeed = {
        min: 30,
        max: 60
    },
    shootingStarOpacityDelta = 0.01,
    shootingStarEmittingInterval = 6000,
    shootingStarLifeTime = 400,
    shootingStarRadius = 2,
    width = window.innerWidth,
    height = window.innerHeight,
    maxTrailLength = 300,
    trailLengthDelta = 0.01;

function lineToAngle(x1, y1, length, radians) {
    var x2 = x1 + length * Math.cos(radians),
        y2 = y1 + length * Math.sin(radians);
    return {
        x: x2,
        y: y2
    };
}

function randomRange(min, max) {
    return min + Math.random() * (max - min);
}

function degreesToRads(degrees) {
    return degrees / 180 * Math.PI;
}

function createShootingStar() {
    var shootingStar = particle.create(randomRange(0, width), randomRange(0, height / 1.5), 0, 0);
    shootingStar.setSpeed(randomRange(shootingStarSpeed.min, shootingStarSpeed.max));
    shootingStar.setHeading(degreesToRads(randomRange(20, 160)));
    shootingStar.radius = shootingStarRadius;
    shootingStar.opacity = 0;
    shootingStar.trailLengthDelta = 0;
    shootingStar.isSpawning = true;
    shootingStar.isDying = false;
    shootingStars.push(shootingStar);
}

function killShootingStar(shootingStar) {
    setTimeout(function () {
        shootingStar.isDying = true;
    }, shootingStarLifeTime);
}

function drawShootingStar(p) {
    var x = p.x,
        y = p.y,
        currentTrailLength = (maxTrailLength * p.trailLengthDelta),
        pos = lineToAngle(x, y, -currentTrailLength, p.getHeading());
    context.fillStyle = "rgba(255, 255, 255, " + p.opacity + ")";
    // context.beginPath();
    // context.arc(x, y, p.radius, 0, Math.PI * 2, false);
    // context.fill();
    var starLength = 5;
    context.beginPath();
    context.moveTo(x - 1, y + 1);
    context.lineTo(x, y + starLength);
    context.lineTo(x + 1, y + 1);
    context.lineTo(x + starLength, y);
    context.lineTo(x + 1, y - 1);
    context.lineTo(x, y + 1);
    context.lineTo(x, y - starLength);
    context.lineTo(x - 1, y - 1);
    context.lineTo(x - starLength, y);
    context.lineTo(x - 1, y + 1);
    context.lineTo(x - starLength, y);
    context.closePath();
    context.fill();
    //trail
    context.fillStyle = "rgba(255, 200, 255, " + p.opacity + ")";
    context.beginPath();
    context.moveTo(x - 1, y - 1);
    context.lineTo(pos.x, pos.y);
    context.lineTo(x + 1, y + 1);
    context.closePath();
    context.fill();
}

var particle = {
    x: 0,
    y: 0,
    vx: 0,
    vy: 0,
    radius: 0,
    create: function (x, y, speed, direction) {
        var obj = Object.create(this);
        obj.x = x;
        obj.y = y;
        obj.vx = Math.cos(direction) * speed;
        obj.vy = Math.sin(direction) * speed;
        return obj;
    },
    getSpeed: function () {
        return Math.sqrt(this.vx * this.vx + this.vy * this.vy);
    },
    setSpeed: function (speed) {
        var heading = this.getHeading();
        this.vx = Math.cos(heading) * speed;
        this.vy = Math.sin(heading) * speed;
    },
    getHeading: function () {
        return Math.atan2(this.vy, this.vx);
    },
    setHeading: function (heading) {
        var speed = this.getSpeed();
        this.vx = Math.cos(heading) * speed;
        this.vy = Math.sin(heading) * speed;
    },
    update: function () {
        this.x += this.vx;
        this.y += this.vy;
    }
};

function drawSpace() {
    // Calculate the screen size
    screenH = $(window).height();
    screenW = $(window).width();

    // Get the canvas
    canvas = $('#space');

    // Fill out the canvas
    canvas.attr('height', screenH);
    canvas.attr('width', screenW);
    context = canvas[0].getContext('2d');

    // Create all the stars
    for (var i = 0; i < numStars; i++) {
        var x = Math.round(Math.random() * screenW);
        var y = Math.round(Math.random() * screenH);
        var length = 1 + Math.random() * 2;
        var opacity = Math.random();

        // Create a new star and draw
        var star = new Star(x, y, length, opacity);

        // Add the the stars array
        stars.push(star);
    }

    timer = setInterval(animate, 1000 / fps);
    setInterval(function () {
        createShootingStar();
    }, shootingStarEmittingInterval);
}

function clearSpace() {
    clearInterval(timer);
    stars = [];
    context.clearRect(0, 0, canvas.width, canvas.height);
}

/**
 * Animate the canvas
 */
function animate() {
    context.clearRect(0, 0, screenW, screenH);
    $.each(stars, function () {
        this.draw(context);
    });
    for (i = 0; i < shootingStars.length; i += 1) {
        var shootingStar = shootingStars[i];
        if (shootingStar.isSpawning) {
            shootingStar.opacity += shootingStarOpacityDelta;
            if (shootingStar.opacity >= 1.0) {
                shootingStar.isSpawning = false;
                killShootingStar(shootingStar);
            }
        }
        if (shootingStar.isDying) {
            shootingStar.opacity -= shootingStarOpacityDelta;
            if (shootingStar.opacity <= 0.0) {
                shootingStar.isDying = false;
                shootingStar.isDead = true;
            }
        }
        shootingStar.trailLengthDelta += trailLengthDelta;
        shootingStar.update();
        if (shootingStar.opacity > 0.0) {
            drawShootingStar(shootingStar);
        }
    }
    for (i = shootingStars.length - 1; i >= 0; i--) {
        if (shootingStars[i].isDead) {
            shootingStars.splice(i, 1);
        }
    }
}

/**
 * Star
 * 
 * @param int x
 * @param int y
 * @param int length
 * @param opacity
 */
function Star(x, y, length, opacity) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    this.length = parseInt(length);
    this.opacity = opacity;
    this.factor = 1;
    this.increment = Math.random() * .03;
}

/**
 * Draw a star
 * 
 * This function draws a start.
 * You need to give the contaxt as a parameter 
 * 
 * @param context
 */
Star.prototype.draw = function () {
    context.rotate((Math.PI * 1 / 10));

    // Save the context
    context.save();

    // move into the middle of the canvas, just to make room
    context.translate(this.x, this.y);

    // Change the opacity
    if (this.opacity > 1) {
        this.factor = -1;
    } else if (this.opacity <= 0) {
        this.factor = 1;

        this.x = Math.round(Math.random() * screenW);
        this.y = Math.round(Math.random() * screenH);
    }

    this.opacity += this.increment * this.factor;

    context.beginPath();

    if (this.length == 2) {
        context.arc(this.x, this.y, 1, 0, Math.PI * 2, false);
    }

    if (this.length == 1) {
        for (var i = 5; i--; ) {
            context.lineTo(0, 1);
            context.translate(0, 1);
            context.rotate((Math.PI * 2 / 10));
            context.lineTo(0, -1);
            context.translate(0, -1);
            context.rotate(-(Math.PI * 6 / 10));
        }

        context.lineTo(0, this.length);
    }

    context.closePath();
    context.fillStyle = "rgba(255, 200, 255, " + this.opacity + ")";
    context.shadowBlur = 5;
    context.shadowColor = '#3333ff';
    context.fill();

    context.restore();
}

$(function () {
    //drawSpace();
//    $(window).on({
//        resize: function () {
//            clearSpace();
//            drawSpace();
//        }
//    });
    $(document).on('click', '.page-links a', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        $('.page').fadeOut(100, 'swing', function () {
            $.ajax({
                url: url,
                type: 'GET',
                success: function (data) {
                    $('.page').html(data);
                    $('.page').fadeIn(100);
                },
            });
        });
    });
    $(document).on('click', '#showBugReportModal', function (e) {
        e.preventDefault();
        $('#bug-report-modal').modal('show');
    });
    $(document).on('submit', '#bug-report-form', function (e) {
        e.preventDefault();
        $.ajax({
            url: $('#bug-report-form').attr('action'),
            type: 'POST',
            data: $('#bug-report-form').serialize(),
            success: function (data) {
                if (data.success) {
                    $('#bug-report-modal').modal('hide');
                    alert('Сообщение отправлено!');
                } else {
                    alert('Ошибка!');
                }
            },
        });
    });
});