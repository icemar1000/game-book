<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $placeLinks app\models\AR\Action[] */
/* @var $convertLinks array */
?>
<ul class="page-links">
    <?php foreach ($placeLinks as $link): ?>
        <li>
            <?php if (isset($convertLinks[$link->to_id])): ?>
                <?= Html::a($link->text, '#', [
                    'data-id' => $convertLinks[$link->to_id],
                ]) ?>
            <?php else: ?>
                <div class="empty-link">
                    <?= $link->text ?>
                </div>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>