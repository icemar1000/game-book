<?php

use yii\helpers\Html;
use app\models\AR\TextBuild;

/* @var $this yii\web\View */
/* @var $pages app\models\AR\Page[] */
/* @var $build app\models\AR\TextBuild */
/* @var $links array */

$tags = $build->getOptionValue(TextBuild::OPTION_TAGS_LINKS_ON_TOP);
?>

<div class="book">
    <h1 class="text-center">SpaceRace</h1>

    <?php foreach ($pages as $number => $page): ?>
        <div class="page" data-id="<?= $number ?>">
            <?php $_links = $page->getLinksByPosition($tags); ?>

            <?php if ($_links['top']): ?>
                <?= $this->render('_links', [
                    'placeLinks'   => $_links['top'],
                    'convertLinks' => $links,
                ]) ?>
            <?php endif; ?>

            <div class="page-text">
                <?= $page->text ?>
            </div>

            <?php if ($_links['bottom']): ?>
                <?= $this->render('_links', [
                    'placeLinks'   => $_links['bottom'],
                    'convertLinks' => $links,
                ]) ?>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>
