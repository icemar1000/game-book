<?php

namespace app\components\helpers;

use app\models\AR\Tag;
use app\models\AR\Page;

class TextCounter
{
    /**
     * @return int
     */
    public static function getCharsCount()
    {
        $count = 0;
        $query = Page::find()
            ->joinWith([
                'tags',
                'outputLinks',
            ]);
        $tag   = Tag::find()
            ->where([
                'title' => 'Первый релиз',
            ])
            ->one();

        if ($tag !== null) {
            $query->where([
                'tag.id' => $tag->id,
            ]);
        }

        $models = $query->all();

        foreach ($models as $model) {
            $raw = strip_tags($model->text);
            $str = preg_replace('/[^a-zA-Zа-яА-Я0-9]/ui', '', $raw);
            $count += strlen($str);

            foreach ($model->outputLinks as $link) {
                $raw = strip_tags($link->text);
                $str = preg_replace('/[^a-zA-Zа-яА-Я0-9]/ui', '', $raw);
                $count += strlen($str);
            }
        }

        return $count / 2;
    }
}
