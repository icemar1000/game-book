<?php

namespace app\components\helpers;

use Yii;
use vova07\console\ConsoleRunner as Runner;

class ConsoleRunner extends Runner
{
    /**
     * @inheritdoc
     */
    public function run($cmd)
    {
        $isWindows = $this->isWindows();
        $phpExecutable = $isWindows && self::getPHPExecutableFromPath()
            ? self::getPHPExecutableFromPath()
            : PHP_BINDIR . '/php';
        $cmd = $phpExecutable . ' ' . Yii::getAlias($this->file) . ' ' . $cmd;

        if ($isWindows === true) {
            pclose(popen('start /b ' . $cmd, 'r'));
        } else {
            pclose(popen($cmd . ' /dev/null &', 'r'));
        }

        return true;
    }

    /**
     * @return boolean|string
     */
    public static function getPHPExecutableFromPath()
    {
        $paths = explode(PATH_SEPARATOR, getenv('PATH'));

        foreach ($paths as $path) {
            if (strstr($path, 'php.exe') && isset($_SERVER["WINDIR"]) && file_exists($path) && is_file($path)) {
                return $path;
            } else {
                $phpSuffix = isset($_SERVER["WINDIR"]) ? ".exe" : "";
                $phpExecutable = $path . DIRECTORY_SEPARATOR . "php" . $phpSuffix;

                if (file_exists($phpExecutable) && is_file($phpExecutable)) {
                    return $phpExecutable;
                }
            }
        }

        return false;
    }
}
