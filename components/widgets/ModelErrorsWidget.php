<?php

namespace app\components\widgets;

use yii\base\Widget;
use app\components\widgets\ActiveForm;

class ModelErrorsWidget extends Widget
{
    public $model;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $form = ActiveForm::begin([
            'cutFormTag' => true,
        ]);

        echo $form->errorSummary($this->model);

        ActiveForm::end();
    }
}
