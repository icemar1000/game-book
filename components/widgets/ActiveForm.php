<?php

namespace app\components\widgets;

use Yii;
use yii\base\InvalidCallException;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm as BaseWidget;
use yii\widgets\ActiveFormAsset;

class ActiveForm extends BaseWidget
{
    public $cutFormTag = false;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        $content = ob_get_clean();

        if (!$this->cutFormTag) {
            echo Html::beginForm($this->action, $this->method, $this->options);
        }

        echo $content;

        if ($this->enableClientScript) {
            $id = $this->options['id'];
            $options = Json::htmlEncode($this->getClientOptions());
            $attributes = Json::htmlEncode($this->attributes);
            $view = $this->getView();
            ActiveFormAsset::register($view);
            $view->registerJs("jQuery('#$id').yiiActiveForm($attributes, $options);");
        }

        if (!$this->cutFormTag) {
            echo Html::endForm();
        }
    }
}
