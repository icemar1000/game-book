<?php

namespace app\components\widgets\imperavi;

use vova07\imperavi\Asset as AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@app/components/widgets/imperavi/assets';
    public $js = [
        'redactor.js',
    ];
}
