<?php

namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\models\AR\AdminLog;

class AdminLogBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
	return [
            ActiveRecord::EVENT_AFTER_INSERT => 'create',
            ActiveRecord::EVENT_AFTER_UPDATE => 'update',
            ActiveRecord::EVENT_AFTER_DELETE => 'delete',
	];
    }

    public function create()
    {
        $this->addLog(AdminLog::ACTION_CREATE);
    }

    public function update()
    {
        $this->addLog(AdminLog::ACTION_UPDATE);
    }

    public function delete()
    {
        $this->addLog(AdminLog::ACTION_DELETE);
    }

    /**
     * @param string $event
     */
    private function addLog($event)
    {
        if (Yii::$app->user->isGuest) {
            return;
        }

        $model = new AdminLog;

        $model->at       = time();
        $model->model    = $this->owner->className();
        $model->action   = $event;
        $model->user_id  = Yii::$app->user->id;
        $model->model_id = $this->owner->id;

        $model->save();
    }
}