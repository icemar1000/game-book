<?php

namespace app\components\behaviors;

use yii\base\Behavior;

class IdForSaveBehavior extends Behavior
{
    private $_id = null;

    /**
     * @return int|null
     */
    public function getIdForSave()
    {
        if (!$this->owner->isNewRecord) {
            return $this->owner->id;
        }
        return $this->_id;
    }

    /**
     * @param int $id
     */
    public function setIdForSave($id)
    {
        $this->_id = $id;
    }
}