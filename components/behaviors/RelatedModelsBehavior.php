<?php

namespace app\components\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class RelatedModelsBehavior extends Behavior
{
    private $_relatedStorage = [];

    public $relations = [];

    /**
     * @inheritdoc
     */
    public function events()
    {
	return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'validateRelated',
            ActiveRecord::EVENT_AFTER_INSERT   => 'saveRelated',
            ActiveRecord::EVENT_AFTER_UPDATE   => 'saveRelated',
	];
    }

    public function init() {
        foreach ($this->relations as $related) {
            $this->_relatedStorage[$related['relation']] = null;
        }
        parent::init();
    }

    /**
     * @param string $name
     * @return array
     * @throws InvalidParamException
     */
    public function getRelated($name)
    {
        foreach ($this->relations as $related) {
            if ($related['relation'] == $name) {
                return $this->_relatedStorage[$name] === null
                    ? $this->owner->{$name}
                    : $this->_relatedStorage[$name];
            }
        }

        throw new InvalidParamException("Relation '$name' not found.");
    }

    /**
     * @param array $data the data array to load, typically `$_POST` or `$_GET`.
     * @param string $formName the form name to use to load the data into the model.
     * @return bool whether `load()` found the expected form in `$data`.
     */
    public function loadWithRelated($data, $formName = null) {
        $parent = $this->owner->load($data, $formName);

        if ($parent) {
            foreach ($this->relations as $related) {
                $this->loadRelated($related['class'], $related['relation'], $related['attribute'], $data);
            }
        }

        return $parent;
    }

    /**
     * @return bool
     */
    private function isRelatedValidationOrSaveNeed()
    {
        $tmp = true;

        foreach ($this->relations as $related) {
            $tmp = $tmp && $this->_relatedStorage[$related['relation']] === null;
        }

        return !$tmp;
    }

    /**
     * @inheritdoc
     */
    public function validateRelated() {
        if ($this->isRelatedValidationOrSaveNeed()) {
            foreach ($this->relations as $related) {
                if (!$related['class']::validateMultiple($this->_relatedStorage[$related['relation']])) {
                    $this->owner->addError($related['relation'], $related['errorMsg']);
                }
            }
        }
    }

    /**
     * @var string $class
     * @var string $relation
     * @var string $attribute
     * @var array $post
     * @return bool
     */
    private function loadRelated($class, $relation, $attribute, $post)
    {
        $this->_relatedStorage[$relation] = [];
        $_class = (new $class())->formName();

        if (isset($post[$_class])) {
            $this->_relatedStorage[$relation] = [];

            foreach ($post[$_class] as $key => $model) {
                $this->_relatedStorage[$relation][$key] = new $class();

                if (!$this->owner->isNewRecord) {
                    $this->_relatedStorage[$relation][$key]->{$attribute} = $this->owner->id;
                }
            }

            return $class::loadMultiple($this->_relatedStorage[$relation], $post);
        }

        return true;
    }

    public function saveRelated() {
        foreach ($this->relations as $related) {
            if ($this->isRelatedValidationOrSaveNeed()) {
                $this->saveStoredAndDeleteOther($related);
            }
        }
    }

    /**
     * @var array $config
     * @return bool
     */
    private function saveStoredAndDeleteOther($config)
    {
        $class     = $config['class'];
        $storage   = $config['relation'];
        $attribute = $config['attribute'];
        $with      = isset($config['with']) ? $config['with'] : [];

        $ids = [];

        foreach ($this->_relatedStorage[$storage] as $model) {
            $_model = null;

            if ($model->idForSave && ($_model = $class::findOne($model->idForSave)) !== null) {
                //Костыль
                $_model->load([
                    $model->formName() => $model->getAttributes(),
                ]);
            }

            $forSave = $_model !== null ? $_model : $model;
            $forSave->{$attribute} = $this->owner->id;

            foreach ($with as $_attribute) {
                $forSave->{$_attribute} = $model->{$_attribute};
            }

            $forSave->save(false);

            $ids[] = $forSave->id;
        }

        $condition = !empty($ids) ? [
            'and',
            "$attribute = :id",
            ['not in', 'id', $ids],
        ] : "$attribute = :id";

        return $class::deleteAll($condition, [':id' => $this->owner->id]);
    }
}