<?php

namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use app\components\widgets\ActiveForm;

class FlashMessagesBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
	return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'error',
            ActiveRecord::EVENT_AFTER_INSERT   => 'success',
            ActiveRecord::EVENT_AFTER_UPDATE   => 'success',
            ActiveRecord::EVENT_AFTER_DELETE   => 'warning',
	];
    }

    public function error()
    {
        if ($this->owner->hasErrors()) {
            $form = ActiveForm::begin([
                'cutFormTag' => true,
            ]);

            Yii::$app->getSession()->setFlash('error', $form->errorSummary($this->owner));

            ActiveForm::end();
        }
    }

    public function success()
    {
        Yii::$app->getSession()->setFlash('success', 'Запись успешно сохранена.');
    }

    public function warning()
    {
        Yii::$app->getSession()->setFlash('warning', 'Запись успешно удалена.');
    }
}